package com.rayels.camerbus.entities;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.*;

@Entity
public class Entreprise implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private String nomCommercial;
   private String registre;
   private String adresse;
   private String codePostal;
   private String telephone;
   private String email;
   private String siteweb;
   private double capital;
   private String regime;
   private String activite;
   private String branche;
   private String logo;
   private String note;
   private Date dateCreation;

   public Entreprise() {
   }

   public Entreprise(Long id, String nomCommercial, String registre, String adresse, String codePostal, String telephone, String email, String siteweb, double capital, String regime, String activite, String branche, String logo, String note, Date dateCreation) {
      this.id = id;
      this.nomCommercial = nomCommercial;
      this.registre = registre;
      this.adresse = adresse;
      this.codePostal = codePostal;
      this.telephone = telephone;
      this.email = email;
      this.siteweb = siteweb;
      this.capital = capital;
      this.regime = regime;
      this.activite = activite;
      this.branche = branche;
      this.logo = logo;
      this.note = note;
      this.dateCreation = dateCreation;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getNomCommercial() {
      return nomCommercial;
   }

   public void setNomCommercial(String nomCommercial) {
      this.nomCommercial = nomCommercial;
   }

   public String getRegistre() {
      return registre;
   }

   public void setRegistre(String registre) {
      this.registre = registre;
   }

   public String getAdresse() {
      return adresse;
   }

   public void setAdresse(String adresse) {
      this.adresse = adresse;
   }

   public String getCodePostal() {
      return codePostal;
   }

   public void setCodePostal(String codePostal) {
      this.codePostal = codePostal;
   }

   public String getTelephone() {
      return telephone;
   }

   public void setTelephone(String telephone) {
      this.telephone = telephone;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getSiteweb() {
      return siteweb;
   }

   public void setSiteweb(String siteweb) {
      this.siteweb = siteweb;
   }

   public double getCapital() {
      return capital;
   }

   public void setCapital(double capital) {
      this.capital = capital;
   }

   public String getRegime() {
      return regime;
   }

   public void setRegime(String regime) {
      this.regime = regime;
   }

   public String getActivite() {
      return activite;
   }

   public void setActivite(String activite) {
      this.activite = activite;
   }

   public String getBranche() {
      return branche;
   }

   public void setBranche(String branche) {
      this.branche = branche;
   }

   public String getLogo() {
      return logo;
   }

   public void setLogo(String logo) {
      this.logo = logo;
   }

   public String getNote() {
      return note;
   }

   public void setNote(String note) {
      this.note = note;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }
}