package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Colis implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private double poids;
   private String numero;
   private String couleur;
   private String libelle;
   private String codeBar;
   private double prixFret;
   private double longueur;
   private double largeur;
   private double hauteur;
   private Date dateCreation;

   @ManyToOne
   private Ticket ticket;

   public Colis() {
   }

   public Colis(Long id, double poids, String numero, String couleur, String libelle, String codeBar, double prixFret, double longueur, double largeur, double hauteur, Date dateCreation, Ticket ticket) {
      this.id = id;
      this.poids = poids;
      this.numero =numero;
      this.couleur = couleur;
      this.libelle = libelle;
      this.codeBar = codeBar;
      this.prixFret = prixFret;
      this.longueur = longueur;
      this.largeur = largeur;
      this.hauteur = hauteur;
      this.dateCreation = dateCreation;
      this.ticket = ticket;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public double getPoids() {
      return poids;
   }

   public void setPoids(double poids) {
      this.poids = poids;
   }

   public String getCouleur() {
      return couleur;
   }

   public void setCouleur(String couleur) {
      this.couleur = couleur;
   }

   public String getLibelle() {
      return libelle;
   }

   public void setLibelle(String libelle) {
      this.libelle = libelle;
   }

   public String getCodeBar() {
      return codeBar;
   }

   public void setCodeBar(String codeBar) {
      this.codeBar = codeBar;
   }

   public double getPrixFret() {
      return prixFret;
   }

   public void setPrixFret(double prixFret) {
      this.prixFret = prixFret;
   }

   public double getLongueur() {
      return longueur;
   }

   public void setLongueur(double longueur) {
      this.longueur = longueur;
   }

   public double getLargeur() {
      return largeur;
   }

   public void setLargeur(double largeur) {
      this.largeur = largeur;
   }

   public double getHauteur() {
      return hauteur;
   }

   public void setHauteur(double hauteur) {
      this.hauteur = hauteur;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Ticket getTicket() {
      return ticket;
   }

   public void setTicket(Ticket ticket) {
      this.ticket = ticket;
   }

   public String getNumero() {
      return numero;
   }

   public void setNumero(String numero) {
      this.numero = numero;
   }
}