package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Ticket implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private String codeQR;
   private Date dateDepart;
   private Date dateArrivee;
   private int numero;
   private double prix;
   private String codePaiement;
   private boolean reserve;
   private boolean cloture;
   private String nomClient;
   private Date dateCreation;
   private Date dateReservation;
   private Date dateExpiration;
   private String note;
   private String note2;
   private boolean utilise;
   private double frais;
   private boolean annule;
   private boolean reprogramme;
   private Date dateRepro;
   private Date dateAnnulation;
   @ManyToOne
   private ModePaiement modePaiement;
   @ManyToOne
   private Utilisateur utilisateur;
   @OneToMany(mappedBy = "ticket")
   private Collection<Colis> colis;
   @ManyToOne
   private Voyage voyage;
   @OneToOne
   private Point point;
   @OneToOne
   private BonAchat bonAchat;

   public Ticket(Long id, String codeQR, Date dateDepart, Date dateArrivee, int numero, double prix, String codePaiement, boolean reserve, boolean cloture, String nomClient, Date dateCreation, Date dateReservation, Date dateExpiration, String note, String note2, boolean utilise, double frais, boolean annule, boolean reprogramme, Date dateRepro, Date dateAnnulation, ModePaiement modePaiement, Utilisateur utilisateur, Collection<Colis> colis, Voyage voyage, Point point, BonAchat bonAchat) {
      this.id = id;
      this.codeQR = codeQR;
      this.dateDepart = dateDepart;
      this.dateArrivee = dateArrivee;
      this.numero = numero;
      this.prix = prix;
      this.codePaiement = codePaiement;
      this.reserve = reserve;
      this.cloture = cloture;
      this.nomClient = nomClient;
      this.dateCreation = dateCreation;
      this.dateReservation = dateReservation;
      this.dateExpiration = dateExpiration;
      this.note = note;
      this.note2 = note2;
      this.utilise = utilise;
      this.frais = frais;
      this.annule = annule;
      this.reprogramme = reprogramme;
      this.dateRepro = dateRepro;
      this.dateAnnulation = dateAnnulation;
      this.modePaiement = modePaiement;
      this.utilisateur = utilisateur;
      this.colis = colis;
      this.voyage = voyage;
      this.point = point;
      this.bonAchat = bonAchat;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getCodeQR() {
      return codeQR;
   }

   public void setCodeQR(String codeQR) {
      this.codeQR = codeQR;
   }

   public Date getDateDepart() {
      return dateDepart;
   }

   public void setDateDepart(Date dateDepart) {
      this.dateDepart = dateDepart;
   }

   public Date getDateArrivee() {
      return dateArrivee;
   }

   public void setDateArrivee(Date dateArrivee) {
      this.dateArrivee = dateArrivee;
   }

   public int getNumero() {
      return numero;
   }

   public void setNumero(int numero) {
      this.numero = numero;
   }

   public double getPrix() {
      return prix;
   }

   public void setPrix(double prix) {
      this.prix = prix;
   }

   public String getCodePaiement() {
      return codePaiement;
   }

   public void setCodePaiement(String codePaiement) {
      this.codePaiement = codePaiement;
   }

   public boolean isReserve() {
      return reserve;
   }

   public void setReserve(boolean reserve) {
      this.reserve = reserve;
   }

   public boolean isCloture() {
      return cloture;
   }

   public void setCloture(boolean cloture) {
      this.cloture = cloture;
   }

   public String getNomClient() {
      return nomClient;
   }

   public void setNomClient(String nomClient) {
      this.nomClient = nomClient;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Date getDateReservation() {
      return dateReservation;
   }

   public void setDateReservation(Date dateReservation) {
      this.dateReservation = dateReservation;
   }

   public Date getDateExpiration() {
      return dateExpiration;
   }

   public void setDateExpiration(Date dateExpiration) {
      this.dateExpiration = dateExpiration;
   }

   public String getNote() {
      return note;
   }

   public void setNote(String note) {
      this.note = note;
   }

   public String getNote2() {
      return note2;
   }

   public void setNote2(String note2) {
      this.note2 = note2;
   }

   public boolean isUtilise() {
      return utilise;
   }

   public void setUtilise(boolean utilise) {
      this.utilise = utilise;
   }

   public double getFrais() {
      return frais;
   }

   public void setFrais(double frais) {
      this.frais = frais;
   }

   public boolean isAnnule() {
      return annule;
   }

   public void setAnnule(boolean annule) {
      this.annule = annule;
   }

   public boolean isReprogramme() {
      return reprogramme;
   }

   public void setReprogramme(boolean reprogramme) {
      this.reprogramme = reprogramme;
   }

   public Date getDateRepro() {
      return dateRepro;
   }

   public void setDateRepro(Date dateRepro) {
      this.dateRepro = dateRepro;
   }

   public Date getDateAnnulation() {
      return dateAnnulation;
   }

   public void setDateAnnulation(Date dateAnnulation) {
      this.dateAnnulation = dateAnnulation;
   }

   public ModePaiement getModePaiement() {
      return modePaiement;
   }

   public void setModePaiement(ModePaiement modePaiement) {
      this.modePaiement = modePaiement;
   }

   public Utilisateur getUtilisateur() {
      return utilisateur;
   }

   public void setUtilisateur(Utilisateur utilisateur) {
      this.utilisateur = utilisateur;
   }

   public Collection<Colis> getColis() {
      return colis;
   }

   public void setColis(Collection<Colis> colis) {
      this.colis = colis;
   }

   public Voyage getVoyage() {
      return voyage;
   }

   public void setVoyage(Voyage voyage) {
      this.voyage = voyage;
   }

   public Point getPoint() {
      return point;
   }

   public void setPoint(Point point) {
      this.point = point;
   }

   public BonAchat getBonAchat() {
      return bonAchat;
   }

   public void setBonAchat(BonAchat bonAchat) {
      this.bonAchat = bonAchat;
   }

   public Ticket() {
   }
}