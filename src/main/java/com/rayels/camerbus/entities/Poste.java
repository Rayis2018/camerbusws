package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Poste implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private String id;
   private String nom;
   private Date dateCreation;

   @OneToMany(mappedBy = "poste")
   public Collection<Utilisateur> utilisateurs;

   public Poste() {
   }

   public String getId() {
      return id;
   }

   public Poste(String id, String nom, Date dateCreation, Collection<Utilisateur> utilisateurs) {
      this.id = id;
      this.nom = nom;
      this.dateCreation = dateCreation;
      this.utilisateurs = utilisateurs;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getNom() {
      return nom;
   }

   public void setNom(String nom) {
      this.nom = nom;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Collection<Utilisateur> getUtilisateurs() {
      return utilisateurs;
   }

   public void setUtilisateurs(Collection<Utilisateur> utilisateurs) {
      this.utilisateurs = utilisateurs;
   }
}