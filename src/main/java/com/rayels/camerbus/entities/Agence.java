package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Agence implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private String nomCommercial;
   private String code;
   private String adresse;
   private String telephone;
   private String fax;
   private String codePostal;
   private String email;
   private String siteweb;
   private double longitude;
   private double latitude;
   private double altitude;
   private Date dateCreation;

   @ManyToOne
   private Ville ville;
   @ManyToOne
   private Entreprise entreprise;
   @OneToMany(mappedBy="agence")
   private Collection<Voyage> voyage;
   @OneToMany(mappedBy="agence")
   private Collection<Utilisateur> utilisateur;

   public Agence(Long id, String nomCommercial, String code, String adresse, String telephone, String fax, String codePostal, String email, String siteweb, double longitude, double latitude, double altitude, Date dateCreation, Ville ville, Entreprise entreprise, Collection<Voyage> voyage, Collection<Utilisateur> utilisateur) {
      this.id = id;
      this.nomCommercial = nomCommercial;
      this.code = code;
      this.adresse = adresse;
      this.telephone = telephone;
      this.fax = fax;
      this.codePostal = codePostal;
      this.email = email;
      this.siteweb = siteweb;
      this.longitude = longitude;
      this.latitude = latitude;
      this.altitude = altitude;
      this.dateCreation = dateCreation;
      this.ville = ville;
      this.entreprise = entreprise;
      this.voyage = voyage;
      this.utilisateur = utilisateur;
   }

   public Agence(){}

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getNomCommercial() {
      return nomCommercial;
   }

   public void setNomCommercial(String nomCommercial) {
      this.nomCommercial = nomCommercial;
   }

   public String getCode() {
      return code;
   }

   public void setCode(String code) {
      this.code = code;
   }

   public String getAdresse() {
      return adresse;
   }

   public void setAdresse(String adresse) {
      this.adresse = adresse;
   }

   public String getTelephone() {
      return telephone;
   }

   public void setTelephone(String telephone) {
      this.telephone = telephone;
   }

   public String getFax() {
      return fax;
   }

   public void setFax(String fax) {
      this.fax = fax;
   }

   public String getCodePostal() {
      return codePostal;
   }

   public void setCodePostal(String codePostal) {
      this.codePostal = codePostal;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getSiteweb() {
      return siteweb;
   }

   public void setSiteweb(String siteweb) {
      this.siteweb = siteweb;
   }

   public double getLongitude() {
      return longitude;
   }

   public void setLongitude(double longitude) {
      this.longitude = longitude;
   }

   public double getLatitude() {
      return latitude;
   }

   public void setLatitude(double latitude) {
      this.latitude = latitude;
   }

   public double getAltitude() {
      return altitude;
   }

   public void setAltitude(double altitude) {
      this.altitude = altitude;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Ville getVille() {
      return ville;
   }

   public void setVille(Ville ville) {
      this.ville = ville;
   }

   public Entreprise getEntreprise() {
      return entreprise;
   }

   public void setEntreprise(Entreprise entreprise) {
      this.entreprise = entreprise;
   }

   public Collection<Voyage> getVoyage() {
      return voyage;
   }

   public void setVoyage(Collection<Voyage> voyage) {
      this.voyage = voyage;
   }

   public Collection<Utilisateur> getUtilisateur() {
      return utilisateur;
   }

   public void setUtilisateur(Collection<Utilisateur> utilisateur) {
      this.utilisateur = utilisateur;
   }
}