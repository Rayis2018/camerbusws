package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Voyage implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private Date dateDepart;
   private Date dateArrivee;
   private double prix;
   private String note;
   @OneToMany(mappedBy = "voyage")
   private Collection<Ticket> ticket;
   @ManyToOne
   private Bus bus;
   @ManyToOne
   private Agence agence;
   @ManyToOne
   private Ville villeDepart;
   @ManyToOne
   private Ville villeArrivee;
   private Date dateCreation;

   public Voyage(Long id, Date dateDepart, Date dateArrivee, double prix, String note, Collection<Ticket> ticket, Bus bus, Agence agence, Ville villeDepart, Ville villeArrivee, Date dateCreation) {
      this.id = id;
      this.dateDepart = dateDepart;
      this.dateArrivee = dateArrivee;
      this.prix = prix;
      this.note = note;
      this.ticket = ticket;
      this.bus = bus;
      this.agence = agence;
      this.villeDepart = villeDepart;
      this.villeArrivee = villeArrivee;
      this.dateCreation = dateCreation;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public Date getDateDepart() {
      return dateDepart;
   }

   public void setDateDepart(Date dateDepart) {
      this.dateDepart = dateDepart;
   }

   public Date getDateArrivee() {
      return dateArrivee;
   }

   public void setDateArrivee(Date dateArrivee) {
      this.dateArrivee = dateArrivee;
   }

   public double getPrix() {
      return prix;
   }

   public void setPrix(double prix) {
      this.prix = prix;
   }

   public String getNote() {
      return note;
   }

   public void setNote(String note) {
      this.note = note;
   }

   public Collection<Ticket> getTicket() {
      return ticket;
   }

   public void setTicket(Collection<Ticket> ticket) {
      this.ticket = ticket;
   }

   public Bus getBus() {
      return bus;
   }

   public void setBus(Bus bus) {
      this.bus = bus;
   }

   public Agence getAgence() {
      return agence;
   }

   public void setAgence(Agence agence) {
      this.agence = agence;
   }

   public Ville getVilleDepart() {
      return villeDepart;
   }

   public void setVilleDepart(Ville villeDepart) {
      this.villeDepart = villeDepart;
   }

   public Ville getVilleArrivee() {
      return villeArrivee;
   }

   public void setVilleArrivee(Ville villeArrivee) {
      this.villeArrivee = villeArrivee;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Voyage() {
   }
}