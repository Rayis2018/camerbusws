package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class BonAchat implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private int id;
   private String numero;
   private String nomClient;
   private String adresse;
   private String telephone;
   private double montant;
   private Date dateValid;
   private Date dateCreation;
   private boolean utilise;

   @OneToOne
   private Ticket ticket;

   public BonAchat() {
   }

   public BonAchat(int id, String numero, String nomClient, String adresse, String telephone, double montant, Date dateValid, Date dateCreation, boolean utilise, Ticket ticket) {
      this.id = id;
      this.numero = numero;
      this.nomClient = nomClient;
      this.adresse = adresse;
      this.telephone = telephone;
      this.montant = montant;
      this.dateValid = dateValid;
      this.dateCreation = dateCreation;
      this.utilise = utilise;
      this.ticket = ticket;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getNumero() {
      return numero;
   }

   public void setNumero(String numero) {
      this.numero = numero;
   }

   public String getNomClient() {
      return nomClient;
   }

   public void setNomClient(String nomClient) {
      this.nomClient = nomClient;
   }

   public String getAdresse() {
      return adresse;
   }

   public void setAdresse(String adresse) {
      this.adresse = adresse;
   }

   public String getTelephone() {
      return telephone;
   }

   public void setTelephone(String telephone) {
      this.telephone = telephone;
   }

   public double getMontant() {
      return montant;
   }

   public void setMontant(double montant) {
      this.montant = montant;
   }

   public Date getDateValid() {
      return dateValid;
   }

   public void setDateValid(Date dateValid) {
      this.dateValid = dateValid;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public boolean isUtilise() {
      return utilise;
   }

   public void setUtilise(boolean utilise) {
      this.utilise = utilise;
   }

   public Ticket getTicket() {
      return ticket;
   }

   public void setTicket(Ticket ticket) {
      this.ticket = ticket;
   }
}