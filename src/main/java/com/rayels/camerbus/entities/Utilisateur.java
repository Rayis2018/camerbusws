package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Utilisateur implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private String nom;
   private String prenom;
   private String adresse;
   private String telephone;
   private String codePostal;
   private String email;
   private String fax;
   private String sexe;
   private String photo;
   private Date dateCreation;

   @ManyToOne
   private Compte compte;
   @ManyToOne
   private Agence agence;
   @OneToMany(mappedBy = "utilisateur")
   private Collection<Ticket> ticket;
   @OneToMany(mappedBy = "utilisateur")
   private Collection<Revendication> revendication;
   @ManyToOne
   private Poste poste;

   public Utilisateur(Long id, String nom, String prenom, String adresse, String telephone,
                      String codePostal, String email, String fax, String sexe, String photo,
                      Date dateCreation, Compte compte, Agence agence, Collection<Ticket> ticket,
                      Collection<Revendication> revendication, Poste poste) {
      this.id = id;
      this.nom = nom;
      this.prenom = prenom;
      this.adresse = adresse;
      this.telephone = telephone;
      this.codePostal = codePostal;
      this.email = email;
      this.fax = fax;
      this.sexe = sexe;
      this.photo = photo;
      this.dateCreation = dateCreation;
      this.compte = compte;
      this.agence = agence;
      this.ticket = ticket;
      this.revendication = revendication;
      this.poste = poste;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getNom() {
      return nom;
   }

   public void setNom(String nom) {
      this.nom = nom;
   }

   public String getPrenom() {
      return prenom;
   }

   public void setPrenom(String prenom) {
      this.prenom = prenom;
   }

   public String getAdresse() {
      return adresse;
   }

   public void setAdresse(String adresse) {
      this.adresse = adresse;
   }

   public String getTelephone() {
      return telephone;
   }

   public void setTelephone(String telephone) {
      this.telephone = telephone;
   }

   public String getCodePostal() {
      return codePostal;
   }

   public void setCodePostal(String codePostal) {
      this.codePostal = codePostal;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getFax() {
      return fax;
   }

   public void setFax(String fax) {
      this.fax = fax;
   }

   public String getSexe() {
      return sexe;
   }

   public void setSexe(String sexe) {
      this.sexe = sexe;
   }

   public String getPhoto() {
      return photo;
   }

   public void setPhoto(String photo) {
      this.photo = photo;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Compte getCompte() {
      return compte;
   }

   public void setCompte(Compte compte) {
      this.compte = compte;
   }

   public Agence getAgence() {
      return agence;
   }

   public void setAgence(Agence agence) {
      this.agence = agence;
   }

   public Collection<Ticket> getTicket() {
      return ticket;
   }

   public void setTicket(Collection<Ticket> ticket) {
      this.ticket = ticket;
   }

   public Collection<Revendication> getRevendication() {
      return revendication;
   }

   public void setRevendication(Collection<Revendication> revendication) {
      this.revendication = revendication;
   }

   public Poste getPoste() {
      return poste;
   }

   public void setPoste(Poste poste) {
      this.poste = poste;
   }

   public Utilisateur() {
   }
}