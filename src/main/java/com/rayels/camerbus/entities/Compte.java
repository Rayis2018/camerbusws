package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Compte implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private int numero;
   private double solde;
   private Date dateCreation;
   private Date dateModif;
   private Date dateAnnul;
   private int nbrePoints;

   @ManyToOne
   public TypeCompte typeCompte;
   @OneToOne
   public Utilisateur utilisateur;
   @ManyToOne
   public Profile profile;

   public Compte() {
   }

   public Long getId() {
      return id;
   }

   public Compte(Long id, int numero, double solde, Date dateCreation, Date dateModif, Date dateAnnul, int nbrePoints, TypeCompte typeCompte, Utilisateur utilisateur, Profile profile) {
      this.id = id;
      this.numero = numero;
      this.solde = solde;
      this.dateCreation = dateCreation;
      this.dateModif = dateModif;
      this.dateAnnul = dateAnnul;
      this.nbrePoints = nbrePoints;
      this.typeCompte = typeCompte;
      this.utilisateur = utilisateur;
      this.profile = profile;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public int getNumero() {
      return numero;
   }

   public void setNumero(int numero) {
      this.numero = numero;
   }

   public double getSolde() {
      return solde;
   }

   public void setSolde(double solde) {
      this.solde = solde;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Date getDateModif() {
      return dateModif;
   }

   public void setDateModif(Date dateModif) {
      this.dateModif = dateModif;
   }

   public Date getDateAnnul() {
      return dateAnnul;
   }

   public void setDateAnnul(Date dateAnnul) {
      this.dateAnnul = dateAnnul;
   }

   public int getNbrePoints() {
      return nbrePoints;
   }

   public void setNbrePoints(int nbrePoints) {
      this.nbrePoints = nbrePoints;
   }

   public TypeCompte getTypeCompte() {
      return typeCompte;
   }

   public void setTypeCompte(TypeCompte typeCompte) {
      this.typeCompte = typeCompte;
   }

   public Utilisateur getUtilisateur() {
      return utilisateur;
   }

   public void setUtilisateur(Utilisateur utilisateur) {
      this.utilisateur = utilisateur;
   }

   public Profile getProfile() {
      return profile;
   }

   public void setProfile(Profile profile) {
      this.profile = profile;
   }
}