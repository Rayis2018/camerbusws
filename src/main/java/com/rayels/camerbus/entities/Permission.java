package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Permission implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private int creation;
   private int lecture;
   private int modification;
   private int suppression;
   private Date dateCreation;

   @OneToMany(mappedBy = "permission")
   private Collection<Profile> profile;
   @OneToMany(mappedBy = "permission")
   private Collection<Fonctionnalite> fonctionnalite;

   public Permission() {
   }

   public Permission(Long id, int creation, int lecture, int modification, int suppression, Date dateCreation, Collection<Profile> profile, Collection<Fonctionnalite> fonctionnalite) {
      this.id = id;
      this.creation = creation;
      this.lecture = lecture;
      this.modification = modification;
      this.suppression = suppression;
      this.dateCreation = dateCreation;
      this.profile = profile;
      this.fonctionnalite = fonctionnalite;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public int getCreation() {
      return creation;
   }

   public void setCreation(int creation) {
      this.creation = creation;
   }

   public int getLecture() {
      return lecture;
   }

   public void setLecture(int lecture) {
      this.lecture = lecture;
   }

   public int getModification() {
      return modification;
   }

   public void setModification(int modification) {
      this.modification = modification;
   }

   public int getSuppression() {
      return suppression;
   }

   public void setSuppression(int suppression) {
      this.suppression = suppression;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Collection<Profile> getProfile() {
      return profile;
   }

   public void setProfile(Collection<Profile> profile) {
      this.profile = profile;
   }

   public Collection<Fonctionnalite> getFonctionnalite() {
      return fonctionnalite;
   }

   public void setFonctionnalite(Collection<Fonctionnalite> fonctionnalite) {
      this.fonctionnalite = fonctionnalite;
   }
}