package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Revendication implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private int id;
   private String nomClient;
   private String email;
   private String telephone;
   private String description;
   private String codeTicket;
   private String couleur;
   private String marque;
   private Date dateCreation;

   @ManyToOne
   public Utilisateur utilisateur;

   public Revendication(int id, String nomClient, String email, String telephone, String description, String codeTicket, String couleur, String marque, Date dateCreation, Utilisateur utilisateur) {
      this.id = id;
      this.nomClient = nomClient;
      this.email = email;
      this.telephone = telephone;
      this.description = description;
      this.codeTicket = codeTicket;
      this.couleur = couleur;
      this.marque = marque;
      this.dateCreation = dateCreation;
      this.utilisateur = utilisateur;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getNomClient() {
      return nomClient;
   }

   public void setNomClient(String nomClient) {
      this.nomClient = nomClient;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getTelephone() {
      return telephone;
   }

   public void setTelephone(String telephone) {
      this.telephone = telephone;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public String getCodeTicket() {
      return codeTicket;
   }

   public void setCodeTicket(String codeTicket) {
      this.codeTicket = codeTicket;
   }

   public String getCouleur() {
      return couleur;
   }

   public void setCouleur(String couleur) {
      this.couleur = couleur;
   }

   public String getMarque() {
      return marque;
   }

   public void setMarque(String marque) {
      this.marque = marque;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Utilisateur getUtilisateur() {
      return utilisateur;
   }

   public void setUtilisateur(Utilisateur utilisateur) {
      this.utilisateur = utilisateur;
   }

   public Revendication() {
   }
}