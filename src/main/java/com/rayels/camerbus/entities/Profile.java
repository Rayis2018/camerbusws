package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Profile implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private String nom;
   private Date dateCreation;

   @OneToMany(mappedBy = "profile")
   private Collection<Compte> compte;
   @ManyToOne
   private Permission permission;

   public Profile() {
   }

   public Profile(Long id, String nom, Date dateCreation,
                  Collection<Compte> compte, Permission permission) {
      this.id = id;
      this.nom = nom;
      this.dateCreation = dateCreation;
      this.compte = compte;
      this.permission = permission;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getNom() {
      return nom;
   }

   public void setNom(String nom) {
      this.nom = nom;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Collection<Compte> getCompte() {
      return compte;
   }

   public void setCompte(Collection<Compte> compte) {
      this.compte = compte;
   }

   public Permission getPermission() {
      return permission;
   }

   public void setPermission(Permission permission) {
      this.permission = permission;
   }
}