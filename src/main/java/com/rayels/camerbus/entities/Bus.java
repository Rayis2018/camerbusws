package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Bus implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private int id;
   private String immatriculation;
   private String marque;
   private String modele;
   private int nbrePlaces;
   private String couleur;
   private boolean reserve;
   private double longitude;
   private double latitude;
   private double altitude;
   private Date dateCreation;

   @OneToMany(mappedBy = "bus")
   private Collection<Siege> siege;
   @OneToMany(mappedBy = "bus")
   private Collection<Voyage> voyage;

   public Bus() {
   }

   public Bus(int id, String immatriculation, String marque, String modele, int nbrePlaces, String couleur, boolean reserve, double longitude, double latitude, double altitude, Date dateCreation, Collection<Siege> siege, Collection<Voyage> voyage) {
      this.id = id;
      this.immatriculation = immatriculation;
      this.marque = marque;
      this.modele = modele;
      this.nbrePlaces = nbrePlaces;
      this.couleur = couleur;
      this.reserve = reserve;
      this.longitude = longitude;
      this.latitude = latitude;
      this.altitude = altitude;
      this.dateCreation = dateCreation;
      this.siege = siege;
      this.voyage = voyage;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getImmatriculation() {
      return immatriculation;
   }

   public void setImmatriculation(String immatriculation) {
      this.immatriculation = immatriculation;
   }

   public String getMarque() {
      return marque;
   }

   public void setMarque(String marque) {
      this.marque = marque;
   }

   public String getModele() {
      return modele;
   }

   public void setModele(String modele) {
      this.modele = modele;
   }

   public int getNbrePlaces() {
      return nbrePlaces;
   }

   public void setNbrePlaces(int nbrePlaces) {
      this.nbrePlaces = nbrePlaces;
   }

   public String getCouleur() {
      return couleur;
   }

   public void setCouleur(String couleur) {
      this.couleur = couleur;
   }

   public boolean isReserve() {
      return reserve;
   }

   public void setReserve(boolean reserve) {
      this.reserve = reserve;
   }

   public double getLongitude() {
      return longitude;
   }

   public void setLongitude(double longitude) {
      this.longitude = longitude;
   }

   public double getLatitude() {
      return latitude;
   }

   public void setLatitude(double latitude) {
      this.latitude = latitude;
   }

   public double getAltitude() {
      return altitude;
   }

   public void setAltitude(double altitude) {
      this.altitude = altitude;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Collection<Siege> getSiege() {
      return siege;
   }

   public void setSiege(Collection<Siege> siege) {
      this.siege = siege;
   }

   public Collection<Voyage> getVoyage() {
      return voyage;
   }

   public void setVoyage(Collection<Voyage> voyage) {
      this.voyage = voyage;
   }
}