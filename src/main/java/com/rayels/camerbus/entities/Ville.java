package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Ville implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private String nom;
   private String note;
   private Date dateCreation;

   @ManyToOne
   private Pays pays;
   @OneToMany(mappedBy = "villeDepart")
   private Collection<Voyage> voyagesDepart;
   @OneToMany(mappedBy = "ville")
   private Collection<Agence> agence;
   @OneToMany(mappedBy = "villeArrivee")
   private Collection<Voyage> voyagesArrivee;

   public Ville(Long id, String nom, String note, Date dateCreation, Pays pays, Collection<Voyage> voyagesDepart, Collection<Agence> agence, Collection<Voyage> voyagesArrivee) {
      this.id = id;
      this.nom = nom;
      this.note = note;
      this.dateCreation = dateCreation;
      this.pays = pays;
      this.voyagesDepart = voyagesDepart;
      this.agence = agence;
      this.voyagesArrivee = voyagesArrivee;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getNom() {
      return nom;
   }

   public void setNom(String nom) {
      this.nom = nom;
   }

   public String getNote() {
      return note;
   }

   public void setNote(String note) {
      this.note = note;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Pays getPays() {
      return pays;
   }

   public void setPays(Pays pays) {
      this.pays = pays;
   }

   public Collection<Voyage> getVoyagesDepart() {
      return voyagesDepart;
   }

   public void setVoyagesDepart(Collection<Voyage> voyagesDepart) {
      this.voyagesDepart = voyagesDepart;
   }

   public Collection<Agence> getAgence() {
      return agence;
   }

   public void setAgence(Collection<Agence> agence) {
      this.agence = agence;
   }

   public Collection<Voyage> getVoyagesArrivee() {
      return voyagesArrivee;
   }

   public void setVoyagesArrivee(Collection<Voyage> voyagesArrivee) {
      this.voyagesArrivee = voyagesArrivee;
   }

   public Ville() {
   }
}