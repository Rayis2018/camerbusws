package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Point implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private int nombre;
   private boolean statut;
   private Date dateCreation;
   private Date dateModif;
   @OneToOne
   private Ticket ticket;

   public Point() {
   }

   public Point(Long id, int nombre, boolean statut, Date dateCreation, Date dateModif, Ticket ticket) {
      this.id = id;
      this.nombre = nombre;
      this.statut = statut;
      this.dateCreation = dateCreation;
      this.dateModif = dateModif;
      this.ticket = ticket;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public int getNombre() {
      return nombre;
   }

   public void setNombre(int nombre) {
      this.nombre = nombre;
   }

   public boolean isStatut() {
      return statut;
   }

   public void setStatut(boolean statut) {
      this.statut = statut;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Date getDateModif() {
      return dateModif;
   }

   public void setDateModif(Date dateModif) {
      this.dateModif = dateModif;
   }

   public Ticket getTicket() {
      return ticket;
   }

   public void setTicket(Ticket ticket) {
      this.ticket = ticket;
   }
}