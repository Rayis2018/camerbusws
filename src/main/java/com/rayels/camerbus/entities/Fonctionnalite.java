package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Fonctionnalite implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private String libelle;
   private String note;
   private Date dateCreation;

   private Configuration configuration;
   @ManyToOne
   private Permission permission;

   public Fonctionnalite() {
   }

   public Fonctionnalite(Long id, String libelle, String note, Date dateCreation, Configuration configuration, Permission permission) {
      this.id = id;
      this.libelle = libelle;
      this.note = note;
      this.dateCreation = dateCreation;
      this.configuration = configuration;
      this.permission = permission;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getLibelle() {
      return libelle;
   }

   public void setLibelle(String libelle) {
      this.libelle = libelle;
   }

   public String getNote() {
      return note;
   }

   public void setNote(String note) {
      this.note = note;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Configuration getConfiguration() {
      return configuration;
   }

   public void setConfiguration(Configuration configuration) {
      this.configuration = configuration;
   }

   public Permission getPermission() {
      return permission;
   }

   public void setPermission(Permission permission) {
      this.permission = permission;
   }
}