package com.rayels.camerbus.entities;
import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Pays implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private String nom;
   private Date dateCreation;

   @OneToMany(mappedBy = "pays")
   public Collection<Ville> ville;

   public Pays() {
   }

   public Pays(Long id, String nom, Date dateCreation, Collection<Ville> ville) {
      this.id = id;
      this.nom = nom;
      this.dateCreation = dateCreation;
      this.ville = ville;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getNom() {
      return nom;
   }

   public void setNom(String nom) {
      this.nom = nom;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Collection<Ville> getVille() {
      return ville;
   }

   public void setVille(Collection<Ville> ville) {
      this.ville = ville;
   }
}