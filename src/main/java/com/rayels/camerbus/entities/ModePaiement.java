package com.rayels.camerbus.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class ModePaiement implements Serializable {
   @Id
   @GeneratedValue(strategy= GenerationType.AUTO)
   private Long id;
   private String libelle;
   private Date dateCreation;

   @OneToMany(mappedBy = "modePaiement")
   private Collection<Ticket> ticket;

   public ModePaiement() {
   }

   public ModePaiement(Long id, String libelle, Date dateCreation, Collection<Ticket> ticket) {
      this.id = id;
      this.libelle = libelle;
      this.dateCreation = dateCreation;
      this.ticket = ticket;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getLibelle() {
      return libelle;
   }

   public void setLibelle(String libelle) {
      this.libelle = libelle;
   }

   public Date getDateCreation() {
      return dateCreation;
   }

   public void setDateCreation(Date dateCreation) {
      this.dateCreation = dateCreation;
   }

   public Collection<Ticket> getTicket() {
      return ticket;
   }

   public void setTicket(Collection<Ticket> ticket) {
      this.ticket = ticket;
   }
}