/**
 * 
 */
package com.rayels.camerbus.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

/**
 * @author Roger
 *
 */

@ApiModel(description="Tout sur la ville")
public class VilleRequest {

	@NotNull
	@ApiModelProperty(notes="nom",required=true)
	String nom;

	@NotNull
	@ApiModelProperty(notes="note")
	String note;

	public VilleRequest() {
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
