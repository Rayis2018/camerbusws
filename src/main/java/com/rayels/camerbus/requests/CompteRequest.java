/**
 * 
 */
package com.rayels.camerbus.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author Roger
 *
 */

@ApiModel(description="Tout sur le compte")
public class CompteRequest {

	@NotNull
	@ApiModelProperty(notes="numero du compte",required=true)
	int numero;

	@NotNull
	@ApiModelProperty(notes="Solde du compte")
	@Min(value = 0)
	double solde;

	@NotNull
	@ApiModelProperty(notes="nombre de point")
	@Min(value = 0)
	int nbrePoints;

	@NotNull
	@ApiModelProperty(notes="ID Type de compte", required = true)
	long idTypeCompte;

	@NotNull
	@ApiModelProperty(notes="ID Utilisateur", required = true)
	long idUtilisateur;

	@NotNull
	@ApiModelProperty(notes="ID profile")
	long idProfile;

	public CompteRequest() {
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public int getNbrePoints() {
		return nbrePoints;
	}

	public void setNbrePoints(int nbrePoints) {
		this.nbrePoints = nbrePoints;
	}

	public long getIdTypeCompte() {
		return idTypeCompte;
	}

	public void setIdTypeCompte(long idTypeCompte) {
		this.idTypeCompte = idTypeCompte;
	}

	public long getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(long idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public long getIdProfile() {
		return idProfile;
	}

	public void setIdProfile(long idProfile) {
		this.idProfile = idProfile;
	}
}
