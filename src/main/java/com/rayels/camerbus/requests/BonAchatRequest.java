/**
 * 
 */
package com.rayels.camerbus.requests;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Roger
 *
 */

@ApiModel(description="Tout sur le bon achat")
public class BonAchatRequest {
	@NotNull
	@ApiModelProperty(notes="Nom du bon d'achat",required=true)
	String numero;

	@NotNull
	@ApiModelProperty(notes="nom du Client",required=true)
	String nomClient;
	@NotNull
	@ApiModelProperty(notes="Adresse du client",required=true)
	String adresse;

	@ApiModelProperty(notes="Telephone client")
	String telephone;

	@ApiModelProperty(notes="Montant", required = true)
	@Min(value=0)
	double montant;

	@JsonFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(notes="Date de validité, format: yyyy-MM-dd", required = true)
	Date dateValid;

	@ApiModelProperty(notes="Entreprise ID")
	long idTicket = 0;

	public BonAchatRequest() {
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	public Date getDateValid() {
		return dateValid;
	}

	public void setDateValid(Date dateValid) {
		this.dateValid = dateValid;
	}

	public long getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(long idTicket) {
		this.idTicket = idTicket;
	}
}
