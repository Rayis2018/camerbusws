/**
 * 
 */
package com.rayels.camerbus.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author Roger
 *
 */

@ApiModel(description="Tout sur le colis")
public class ColisRequest {
	@NotNull
	@ApiModelProperty(notes="poids du colis")
	@Min(value = 0)
	double poids;

	@NotNull
	@ApiModelProperty(notes="numero du colis",required=true)
	String numero;

	@ApiModelProperty(notes="couleur du colis")
	String couleur;

	@NotNull
	@ApiModelProperty(notes="libelle du colis",required=true)
	String libelle;

	@ApiModelProperty(notes="code Bar du colis")
	String codeBar;

	@NotNull
	@ApiModelProperty(notes="prix Fret du colis")
	@Min(value = 0)
	double prixFret;

	@NotNull
	@ApiModelProperty(notes="longueur du colis")
	@Min(value = 0)
	double longueur;

	@NotNull
	@ApiModelProperty(notes="largeur du colis")
	@Min(value = 0)
	double largeur;

	@NotNull
	@ApiModelProperty(notes="hauteur du colis")
	@Min(value = 0)
	double hauteur;

	@NotNull
	@ApiModelProperty(notes="ID colis")
	long idTicket;

	public ColisRequest() {
	}

	public double getPoids() {
		return poids;
	}

	public void setPoids(double poids) {
		this.poids = poids;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getCodeBar() {
		return codeBar;
	}

	public void setCodeBar(String codeBar) {
		this.codeBar = codeBar;
	}

	public double getPrixFret() {
		return prixFret;
	}

	public void setPrixFret(double prixFret) {
		this.prixFret = prixFret;
	}

	public double getLongueur() {
		return longueur;
	}

	public void setLongueur(double longueur) {
		this.longueur = longueur;
	}

	public double getLargeur() {
		return largeur;
	}

	public void setLargeur(double largeur) {
		this.largeur = largeur;
	}

	public double getHauteur() {
		return hauteur;
	}

	public void setHauteur(double hauteur) {
		this.hauteur = hauteur;
	}

	public long getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(long idTicket) {
		this.idTicket = idTicket;
	}
}
