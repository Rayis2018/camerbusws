/**
 * 
 */
package com.rayels.camerbus.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

/**
 * @author Roger
 *
 */
@ApiModel(description="all details about entreprise")
public class EntrepriseRequest {
	@NotNull
	@ApiModelProperty(notes="nom Commercial",required=true)
	String nomCommercial;

	@NotNull
	@ApiModelProperty(notes="registre du commerce",required=true)
	String registre;

	@NotNull
	@ApiModelProperty(notes="adresse",required=true)
	String adresse;

	@NotNull
	@ApiModelProperty(notes="code Postal")
	String codePostal;

	@NotNull
	@ApiModelProperty(notes="telephone",required=true)
	String telephone;

	@NotNull
	@ApiModelProperty(notes="email")
	String email;

	@NotNull
	@ApiModelProperty(notes="siteweb")
	String siteweb;

	@NotNull
	@ApiModelProperty(notes="capital")
	double capital;

	@NotNull
	@ApiModelProperty(notes="regime",required=true)
	String regime;

	@NotNull
	@ApiModelProperty(notes="activite")
	String activite;

	@NotNull
	@ApiModelProperty(notes="branche")
	String branche;

	@NotNull
	@ApiModelProperty(notes="logo")
	String logo;

	@NotNull
	@ApiModelProperty(notes="note")
	String note;

	public String getNomCommercial() {
		return nomCommercial;
	}

	public void setNomCommercial(String nomCommercial) {
		this.nomCommercial = nomCommercial;
	}

	public String getRegistre() {
		return registre;
	}

	public void setRegistre(String registre) {
		this.registre = registre;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSiteweb() {
		return siteweb;
	}

	public void setSiteweb(String siteweb) {
		this.siteweb = siteweb;
	}

	public double getCapital() {
		return capital;
	}

	public void setCapital(double capital) {
		this.capital = capital;
	}

	public String getRegime() {
		return regime;
	}

	public void setRegime(String regime) {
		this.regime = regime;
	}

	public String getActivite() {
		return activite;
	}

	public void setActivite(String activite) {
		this.activite = activite;
	}

	public String getBranche() {
		return branche;
	}

	public void setBranche(String branche) {
		this.branche = branche;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
