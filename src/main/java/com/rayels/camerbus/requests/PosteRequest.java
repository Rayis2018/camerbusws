/**
 * 
 */
package com.rayels.camerbus.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

/**
 * @author Roger
 *
 */

@ApiModel(description="Tout sur le poste")
public class PosteRequest {

	@NotNull
	@ApiModelProperty(notes="nom du poste",required=true)
	String nom;

	public PosteRequest() {
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
}
