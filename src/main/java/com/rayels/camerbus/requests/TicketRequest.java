/**
 * 
 */
package com.rayels.camerbus.requests;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Roger
 *
 */
@ApiModel(description="all details about ticket")
public class TicketRequest {
	@NotNull
	@ApiModelProperty(notes="fax de l'agence")
	String fax;

	@NotNull
	@ApiModelProperty(notes="code QR sur le ticket",required=true)
	String codeQR;

	@NotNull
	@ApiModelProperty(notes="numero du ticket", required = true)
	int numero;

	@NotNull
	@ApiModelProperty(notes="prix du ticket",required=true)
	@Min(value = 0)
	double prix;

	@NotNull
	@ApiModelProperty(notes="code Paiement",required=true)
	String codePaiement;

	@NotNull
	@ApiModelProperty(notes="réservé ou pas", required = true)
	boolean reserve;

	@NotNull
	@ApiModelProperty(notes="cloturé ou pas", required = true)
	boolean cloture;

	@NotNull
	@ApiModelProperty(notes="nom du Client",required=true)
	String nomClient;

	@NotNull
	@ApiModelProperty(notes="notes",required=true)
	String note;

	@NotNull
	@ApiModelProperty(notes="autres notes",required=true)
	String note2;

	@NotNull
	@ApiModelProperty(notes="utilisé ou pas", required = true)
	boolean utilise;

	@NotNull
	@ApiModelProperty(notes="frais", required = true)
	double frais;

	@NotNull
	@ApiModelProperty(notes="annulé ou pas", required = true)
	boolean annule;

	@NotNull
	@ApiModelProperty(notes="reprogrammé ou pas", required = true)
	boolean reprogramme;

	@JsonFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(notes="Date de reprogrammation, format: yyyy-MM-dd")
	Date dateRepro;

	@NotNull
	@ApiModelProperty(notes="ID mode de Paiement", required = true)
	long idModePaiement;

	@NotNull
	@ApiModelProperty(notes="ID utilisateur", required = true)
	long idUtilisateur;

	@NotNull
	@ApiModelProperty(notes="ID voyage", required = true)
	long idVoyage;

	@NotNull
	@ApiModelProperty(notes="ID point")
	long idPoint;

	@NotNull
	@ApiModelProperty(notes="ID bon Achat")
	long idBonAchat;

	public TicketRequest() {
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCodeQR() {
		return codeQR;
	}

	public void setCodeQR(String codeQR) {
		this.codeQR = codeQR;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getCodePaiement() {
		return codePaiement;
	}

	public void setCodePaiement(String codePaiement) {
		this.codePaiement = codePaiement;
	}

	public boolean isReserve() {
		return reserve;
	}

	public void setReserve(boolean reserve) {
		this.reserve = reserve;
	}

	public boolean isCloture() {
		return cloture;
	}

	public void setCloture(boolean cloture) {
		this.cloture = cloture;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getNote2() {
		return note2;
	}

	public void setNote2(String note2) {
		this.note2 = note2;
	}

	public boolean isUtilise() {
		return utilise;
	}

	public void setUtilise(boolean utilise) {
		this.utilise = utilise;
	}

	public double getFrais() {
		return frais;
	}

	public void setFrais(double frais) {
		this.frais = frais;
	}

	public boolean isAnnule() {
		return annule;
	}

	public void setAnnule(boolean annule) {
		this.annule = annule;
	}

	public boolean isReprogramme() {
		return reprogramme;
	}

	public void setReprogramme(boolean reprogramme) {
		this.reprogramme = reprogramme;
	}

	public Date getDateRepro() {
		return dateRepro;
	}

	public void setDateRepro(Date dateRepro) {
		this.dateRepro = dateRepro;
	}

	public long getIdModePaiement() {
		return idModePaiement;
	}

	public void setIdModePaiement(long idModePaiement) {
		this.idModePaiement = idModePaiement;
	}

	public long getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(long idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public long getIdVoyage() {
		return idVoyage;
	}

	public void setIdVoyage(long idVoyage) {
		this.idVoyage = idVoyage;
	}

	public long getIdPoint() {
		return idPoint;
	}

	public void setIdPoint(long idPoint) {
		this.idPoint = idPoint;
	}

	public long getIdBonAchat() {
		return idBonAchat;
	}

	public void setIdBonAchat(long idBonAchat) {
		this.idBonAchat = idBonAchat;
	}
}
