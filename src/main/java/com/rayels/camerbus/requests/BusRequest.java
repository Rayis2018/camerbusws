/**
 * 
 */
package com.rayels.camerbus.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author Roger
 *
 */

@ApiModel(description="Tout sur le bus")
public class BusRequest {
	@NotNull
	@ApiModelProperty(notes="immatriculation du bus",required=true)
	String immatriculation;

	@NotNull
	@ApiModelProperty(notes="marque du bus",required=true)
	String marque;
	@NotNull
	@ApiModelProperty(notes="modele du bus",required=true)
	String modele;

	@ApiModelProperty(notes="nbre de Places du bus", required = true)
	@Min(value=15)
	int nbrePlaces;

	@ApiModelProperty(notes="couleur du bus")
	String couleur;

	@ApiModelProperty(notes="bus reservé ou pas")
	boolean reserve;

	@ApiModelProperty(notes="position du bus en longitude")
	@Min(value=0)
	double longitude;

	@ApiModelProperty(notes="position du bus en latitude")
	@Min(value=0)
	double latitude;

	@ApiModelProperty(notes="position du bus en altitude")
	@Min(value=0)
	double altitude;

	public BusRequest() {
	}

	public String getImmatriculation() {
		return immatriculation;
	}

	public void setImmatriculation(String immatriculation) {
		this.immatriculation = immatriculation;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public int getNbrePlaces() {
		return nbrePlaces;
	}

	public void setNbrePlaces(int nbrePlaces) {
		this.nbrePlaces = nbrePlaces;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public boolean isReserve() {
		return reserve;
	}

	public void setReserve(boolean reserve) {
		this.reserve = reserve;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}
}
