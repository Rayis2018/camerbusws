/**
 * 
 */
package com.rayels.camerbus.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

/**
 * @author Roger
 *
 */

@ApiModel(description="Tout sur le libelle")
public class TypeCompteRequest {

	@NotNull
	@ApiModelProperty(notes="libelle",required=true)
	String libelle;

	public TypeCompteRequest() {
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
}
