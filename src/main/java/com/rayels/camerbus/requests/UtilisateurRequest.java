/**
 * 
 */
package com.rayels.camerbus.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

/**
 * @author Roger
 *
 */
@ApiModel(description="all details about user")
public class UtilisateurRequest {
	@NotNull
	@ApiModelProperty(notes="name",required=true)
	String nom;

	@NotNull
	@ApiModelProperty(notes="prenom")
	String prenom;

	@NotNull
	@ApiModelProperty(notes="adresse")
	String adresse;

	@ApiModelProperty(notes="telephone", required = true)
	String telephone;

	@ApiModelProperty(notes="code Postal")
	String codePostal;

	@ApiModelProperty(notes="email")
	String email;

	@ApiModelProperty(notes="fax")
	String fax;

	@ApiModelProperty(notes="sexe", required = true)
	String sexe;

	@ApiModelProperty(notes="fax")
	String photo;

	@NotNull
	@ApiModelProperty(notes="id compte", required = true)
	long idCompte;

	@ApiModelProperty(notes="id agence", required = true)
	long idAgence;

	@ApiModelProperty(notes="fax",required = true)
	long idPoste;

	public UtilisateurRequest() {
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public long getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(long idCompte) {
		this.idCompte = idCompte;
	}

	public long getIdAgence() {
		return idAgence;
	}

	public void setIdAgence(long idAgence) {
		this.idAgence = idAgence;
	}

	public long getIdPoste() {
		return idPoste;
	}

	public void setIdPoste(long idPoste) {
		this.idPoste = idPoste;
	}
}
