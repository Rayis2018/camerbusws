/**
 * 
 */
package com.rayels.camerbus.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author Roger
 *
 */
@ApiModel(description="all details about agence")
public class AgenceRequest {
	@NotNull
	@ApiModelProperty(notes="agence name",required=true)
	String nomCommercial;

	@NotNull
	@ApiModelProperty(notes="agence code",required=true)
	String code;

	@NotNull
	@ApiModelProperty(notes="agence adresse",required=true)
	String adresse;

	@ApiModelProperty(notes="agence telephone")
	String telephone;

	@ApiModelProperty(notes="agence fax")
	String fax;

	@ApiModelProperty(notes="agence postal code")
	String codePostal;

	@ApiModelProperty(notes="agence email")
	String email;

	@ApiModelProperty(notes="agence webside")
	String webside;

	@ApiModelProperty(notes="longitude")
	@Min(value=0)
	double longitude;

	@ApiModelProperty(notes="latitude")
	@Min(value=0)
	double latitude;

	@ApiModelProperty(notes="altitude")
	@Min(value=0)
	double altitude;

	@ApiModelProperty(notes="Entreprise ID")
	long idEntreprise = 0;

	@ApiModelProperty(notes="Ville ID")
	long idVille = 0;

	public AgenceRequest() {
	}

	public String getNomCommercial() {
		return nomCommercial;
	}

	public void setNomCommercial(String nomCommercial) {
		this.nomCommercial = nomCommercial;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAdresse() {
		return adresse;
	}

	public long getIdEntreprise() {
		return idEntreprise;
	}

	public void setIdEntreprise(int idEntreprise) {
		this.idEntreprise = idEntreprise;
	}

	public long getIdVille() {
		return idVille;
	}

	public void setIdVille(int idVille) {
		this.idVille = idVille;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebside() {
		return webside;
	}

	public void setWebside(String webside) {
		this.webside = webside;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}
}
