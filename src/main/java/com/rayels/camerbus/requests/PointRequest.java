/**
 * 
 */
package com.rayels.camerbus.requests;

import com.rayels.camerbus.entities.Ticket;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Roger
 *
 */

@ApiModel(description="Tout sur le point")
public class PointRequest {

	@NotNull
	@ApiModelProperty(notes="nombre de points",required=true)
	int nombre;

	@NotNull
	@ApiModelProperty(notes="nom du pays",required=true)
	boolean statut;

	@NotNull
	@ApiModelProperty(notes="ID ticket",required=true)
	long idTicket;

	public PointRequest() {
	}

	public int getNombre() {
		return nombre;
	}

	public void setNombre(int nombre) {
		this.nombre = nombre;
	}

	public boolean isStatut() {
		return statut;
	}

	public void setStatut(boolean statut) {
		this.statut = statut;
	}

	public long getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(long idTicket) {
		this.idTicket = idTicket;
	}
}
