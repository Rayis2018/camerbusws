/**
 * 
 */
package com.rayels.camerbus.requests;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Roger
 *
 */

@ApiModel(description="Tout sur le voyage")
public class VoyageRequest {

	@JsonFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(notes="Date de départ, format: yyyy-MM-dd", required = true)
	Date dateDepart;

	@JsonFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(notes="Date d'arrivée, format: yyyy-MM-dd")
	Date dateArrivee;

	@ApiModelProperty(notes="prix du voyage", required = true)
	@Min(value=0)
	double prix;

	@ApiModelProperty(notes="note")
	String note;

	@NotNull
	@ApiModelProperty(notes="ID du bus",required=true)
	long idBus;

	@NotNull
	@ApiModelProperty(notes="ID de l'agence",required=true)
	long idAgence;

	@NotNull
	@ApiModelProperty(notes="ID ville Depart",required=true)
	long idVilleDepart;

	@NotNull
	@ApiModelProperty(notes="ID ville Arrivee",required=true)
	long idVilleArrivee;

	public VoyageRequest() {
	}

	public Date getDateDepart() {
		return dateDepart;
	}

	public void setDateDepart(Date dateDepart) {
		this.dateDepart = dateDepart;
	}

	public Date getDateArrivee() {
		return dateArrivee;
	}

	public void setDateArrivee(Date dateArrivee) {
		this.dateArrivee = dateArrivee;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public long getIdBus() {
		return idBus;
	}

	public void setIdBus(long idBus) {
		this.idBus = idBus;
	}

	public long getIdAgence() {
		return idAgence;
	}

	public void setIdAgence(long idAgence) {
		this.idAgence = idAgence;
	}

	public long getIdVilleDepart() {
		return idVilleDepart;
	}

	public void setIdVilleDepart(long idVilleDepart) {
		this.idVilleDepart = idVilleDepart;
	}

	public long getIdVilleArrivee() {
		return idVilleArrivee;
	}

	public void setIdVilleArrivee(long idVilleArrivee) {
		this.idVilleArrivee = idVilleArrivee;
	}
}
