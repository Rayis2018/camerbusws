/**
 * 
 */
package com.rayels.camerbus.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

/**
 * @author Roger
 *
 */
@ApiModel(description="all details about revendication")
public class RevendicationRequest {
	@NotNull
	@ApiModelProperty(notes="nom Client",required=true)
	String nomClient;

	@ApiModelProperty(notes="email")
	String email;

	@NotNull
	@ApiModelProperty(notes="description",required=true)
	String description;

	@ApiModelProperty(notes="telephone", required = true)
	String telephone;

	@ApiModelProperty(notes="code Ticket")
	String codeTicket;

	@ApiModelProperty(notes="couleur du colis")
	String couleur;

	@ApiModelProperty(notes="marque du colis")
	String marque;

	@NotNull
	@ApiModelProperty(notes="utilisateur du colis")
	long idUtilisateur;


	public RevendicationRequest() {
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getCodeTicket() {
		return codeTicket;
	}

	public void setCodeTicket(String codeTicket) {
		this.codeTicket = codeTicket;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public long getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(long idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
}
