/**
 * 
 */
package com.rayels.camerbus.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author Roger
 *
 */

@ApiModel(description="Tout sur le siège du bus")
public class SiegeRequest {
	@NotNull
	@ApiModelProperty(notes="numero",required=true)
	String numero;

	@NotNull
	@ApiModelProperty(notes="position")
	@Min(value=0)
	int position;

	@NotNull
	@ApiModelProperty(notes="note")
	String note;

	@NotNull
	@ApiModelProperty(notes="photo")
	String photo;

	@ApiModelProperty(notes="ID du bus", required = true)
	long idBus;

	public SiegeRequest() {
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public long getIdBus() {
		return idBus;
	}

	public void setIdBus(long idBus) {
		this.idBus = idBus;
	}
}
