/**
 * 
 */
package com.rayels.camerbus.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

/**
 * @author Roger
 *
 */

@ApiModel(description="Tout sur le profile")
public class ProfileRequest {

	@NotNull
	@ApiModelProperty(notes="nom du profile",required=true)
	String nom;

	@NotNull
	@ApiModelProperty(notes="ID permission")
	long idPermission;

	public ProfileRequest() {
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public long getIdPermission() {
		return idPermission;
	}

	public void setIdPermission(long idPermission) {
		this.idPermission = idPermission;
	}
}
