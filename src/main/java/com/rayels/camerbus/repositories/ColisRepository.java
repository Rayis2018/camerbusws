package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Colis;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ColisRepository extends CrudRepository<Colis, Long> {
    public Colis findByNumero(String numero);
    public Colis findByCodeBar(String codeBar);
}
