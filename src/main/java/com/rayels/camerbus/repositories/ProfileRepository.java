package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends CrudRepository<Profile, Long> {
    public Profile findByNom(String nom);
}
