package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.ModePaiement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModePaiementRepository extends CrudRepository<ModePaiement, Long> {
    public ModePaiement findByLibelle(String libelle);
}
