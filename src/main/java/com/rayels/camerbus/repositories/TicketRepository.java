package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Ticket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends CrudRepository<Ticket, Long> {
    public Ticket findByNumero(int numero);
}
