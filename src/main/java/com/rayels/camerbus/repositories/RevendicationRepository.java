package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Revendication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RevendicationRepository extends CrudRepository<Revendication, Long> {
    public Revendication findByNomClient(String nomClient);
}
