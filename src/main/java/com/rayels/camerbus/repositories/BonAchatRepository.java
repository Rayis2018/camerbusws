package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.BonAchat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BonAchatRepository extends CrudRepository<BonAchat, Long> {
    public BonAchat findBonAchatById(long id);
    public BonAchat findBonAchatByNumero(String numero);
}
