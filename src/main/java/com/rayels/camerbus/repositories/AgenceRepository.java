package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Agence;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgenceRepository extends CrudRepository<Agence, Long> {
    public Agence findAgenceByCode(String code);
}
