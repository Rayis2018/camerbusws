package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Entreprise;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntrepriseRepository extends CrudRepository<Entreprise, Long> {
    public Entreprise findByNomCommercial(String nomCommercial);
}
