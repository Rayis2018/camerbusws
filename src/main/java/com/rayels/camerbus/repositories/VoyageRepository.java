package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Ticket;
import com.rayels.camerbus.entities.Voyage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoyageRepository extends CrudRepository<Voyage, Long> {
    public Voyage findByTicket(Ticket ticket);
}
