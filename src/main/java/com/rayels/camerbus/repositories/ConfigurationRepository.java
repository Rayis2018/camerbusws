package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Configuration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigurationRepository extends CrudRepository<Configuration, Long> {

}
