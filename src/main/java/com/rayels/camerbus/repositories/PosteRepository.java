package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Poste;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PosteRepository extends CrudRepository<Poste, Long> {
    public Poste findByNom(String nom);
}
