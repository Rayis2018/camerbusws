package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Fonctionnalite;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FonctionnaliteRepository extends CrudRepository<Fonctionnalite, Long> {
    public Fonctionnalite findByLibelle(String libelle);
}
