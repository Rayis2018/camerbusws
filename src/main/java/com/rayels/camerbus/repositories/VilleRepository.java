package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Ville;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VilleRepository extends CrudRepository<Ville, Long> {
    public Ville findByNom(String nom);
}
