package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Bus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusRepository extends CrudRepository<Bus, Long> {
    public Bus findByImmatriculation(String immatriculation);

}
