package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Point;
import com.rayels.camerbus.entities.Ticket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PointRepository extends CrudRepository<Point, Long> {
    public Point findByTicket(Ticket ticket);
}
