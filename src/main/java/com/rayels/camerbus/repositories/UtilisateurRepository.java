package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Utilisateur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilisateurRepository extends CrudRepository<Utilisateur, Long> {
    public Utilisateur findByNom(String nom);
}
