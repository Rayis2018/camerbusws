package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Siege;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SiegeRepository extends CrudRepository<Siege, Long> {
    public Siege findByNumero(String numero);
}
