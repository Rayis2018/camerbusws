package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.TypeCompte;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeCompteRepository extends CrudRepository<TypeCompte, Long> {
    public TypeCompte findByLibelle(String libelle);
}
