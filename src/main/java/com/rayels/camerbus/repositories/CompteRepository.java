package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Compte;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompteRepository extends CrudRepository<Compte, Long> {
    public Compte findByNumero(int numero);
}
