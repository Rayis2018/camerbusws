package com.rayels.camerbus.repositories;

import com.rayels.camerbus.entities.Pays;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaysRepository extends CrudRepository<Pays, Long> {
    public Pays findByNom(String nom);
}
