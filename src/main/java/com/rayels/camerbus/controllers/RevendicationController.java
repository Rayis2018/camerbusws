package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Revendication;
import com.rayels.camerbus.repositories.RevendicationRepository;
import com.rayels.camerbus.repositories.UtilisateurRepository;
import com.rayels.camerbus.requests.RevendicationRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/revendication")
public class RevendicationController {

    @Autowired
    RevendicationRepository revendicationRepository;
    @Autowired
    UtilisateurRepository utilisateurRepository;

    @GetMapping("/find/all")
    public Iterable<Revendication> getAllRevencidation() {
        return revendicationRepository.findAll();
    }

    @ApiOperation(value="find Revendication by id",response= Revendication.class)
    @GetMapping("/find/{id}")
    public Revendication getRevendicationById(@PathVariable Long id){
        return revendicationRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find Revendication by nom Client",response= Revendication.class)
    @GetMapping("/find/{nomClient}")
    public Revendication getRevendicationByNomClient(@PathVariable String nomClient){
        return revendicationRepository.findByNomClient(nomClient);
    }

    @ApiOperation(value="add new Revendication",response= Revendication.class)
    @PostMapping("/add")
    public Revendication add(@Valid @RequestBody RevendicationRequest revendicationRequest){
        Revendication revendication = new Revendication();
        revendication.setCodeTicket(revendicationRequest.getCodeTicket());
        revendication.setCouleur(revendicationRequest.getCouleur());
        revendication.setDateCreation(new Date());
        revendication.setDescription(revendicationRequest.getDescription());
        revendication.setEmail(revendicationRequest.getEmail());
        revendication.setMarque(revendicationRequest.getMarque());
        revendication.setNomClient(revendicationRequest.getNomClient());
        revendication.setTelephone(revendicationRequest.getTelephone());
        revendication.setUtilisateur(utilisateurRepository.findById(revendicationRequest.getIdUtilisateur()).orElse(null));
        return  revendicationRepository.save(revendication);
    }

    @ApiOperation(value="update Revendication by id",response= Revendication.class)
    @PutMapping("/update/{id}")
    public Revendication update(@PathVariable long id, @Valid @RequestBody RevendicationRequest revendicationRequest){
        Revendication revendication = revendicationRepository.findById(id).orElse(null);
        revendication.setCodeTicket(revendicationRequest.getCodeTicket());
        revendication.setCouleur(revendicationRequest.getCouleur());
        revendication.setDescription(revendicationRequest.getDescription());
        revendication.setEmail(revendicationRequest.getEmail());
        revendication.setMarque(revendicationRequest.getMarque());
        revendication.setNomClient(revendicationRequest.getNomClient());
        revendication.setTelephone(revendicationRequest.getTelephone());
        revendication.setUtilisateur(utilisateurRepository.findById(revendicationRequest.getIdUtilisateur()).orElse(null));
        return  revendicationRepository.save(revendication);
    }

    @ApiOperation(value="delete Revendication by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        revendicationRepository.deleteById(id);
    }
}
