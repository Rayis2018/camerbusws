package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Poste;
import com.rayels.camerbus.repositories.PosteRepository;
import com.rayels.camerbus.requests.PosteRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/poste")
public class PosteController {

    @Autowired
    PosteRepository posteRepository;

    @GetMapping("/find/all")
    public Iterable<Poste> getAllPoste() {
        return posteRepository.findAll();
    }

    @ApiOperation(value="find Poste by id",response= Poste.class)
    @GetMapping("/find/{id}")
    public Poste getPosteById(@PathVariable Long id){
        return posteRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find Poste by name",response= Poste.class)
    @GetMapping("/find/{nom}")
    public Poste getPosteByNom(@PathVariable String nom){
        return posteRepository.findByNom(nom);
    }

    @ApiOperation(value="add new Poste",response= Poste.class)
    @PostMapping("/add")
    public Poste add(@Valid @RequestBody PosteRequest posteRequest){
        Poste poste = new Poste();
        poste.setDateCreation(new Date());
        poste.setNom(posteRequest.getNom());
        return posteRepository.save(poste);
    }

    @ApiOperation(value="update Poste by id",response= Poste.class)
    @PutMapping("/update/{id}")
    public Poste update(@PathVariable long id, @Valid @RequestBody PosteRequest posteRequest){
        Poste poste = posteRepository.findById(id).orElse(null);
        poste.setNom(posteRequest.getNom());
        return posteRepository.save(poste);
    }

    @ApiOperation(value="delete Poste by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        posteRepository.deleteById(id);
    }

}
