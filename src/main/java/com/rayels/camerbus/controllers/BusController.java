package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Bus;
import com.rayels.camerbus.repositories.BusRepository;
import com.rayels.camerbus.requests.BusRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/bus")
public class BusController {

    @Autowired
    BusRepository busRepository;

    @GetMapping("/find/all")
    public Iterable<Bus> getAllBus() {
        return busRepository.findAll();
    }

    @ApiOperation(value="find bus by id",response= Bus.class)
    @GetMapping("/find/{id}")
    public Bus getBusById(@PathVariable Long id){
        return busRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="add new bus",response= Bus.class)
    @PostMapping("/add")
    public Bus add(@Valid @RequestBody BusRequest busRequest){
        Bus bus = new Bus();
        bus.setAltitude(busRequest.getAltitude());
        bus.setCouleur(busRequest.getCouleur());
        bus.setDateCreation(new Date());
        bus.setImmatriculation(busRequest.getImmatriculation());
        bus.setLatitude(busRequest.getLatitude());
        bus.setLongitude(busRequest.getLongitude());
        bus.setMarque(busRequest.getMarque());
        bus.setModele(busRequest.getModele());
        bus.setNbrePlaces(busRequest.getNbrePlaces());
        bus.setReserve(false);
        return busRepository.save(bus);
    }

    @ApiOperation(value="update bus by id",response= Bus.class)
    @PutMapping("/update/{id}")
    public Bus add(@PathVariable long id, @Valid @RequestBody BusRequest busRequest){
        Bus bus = busRepository.findById(id).orElse(null);
        bus.setAltitude(busRequest.getAltitude());
        bus.setCouleur(busRequest.getCouleur());
        bus.setImmatriculation(busRequest.getImmatriculation());
        bus.setLatitude(busRequest.getLatitude());
        bus.setLongitude(busRequest.getLongitude());
        bus.setMarque(busRequest.getMarque());
        bus.setModele(busRequest.getModele());
        bus.setNbrePlaces(busRequest.getNbrePlaces());
        bus.setReserve(busRequest.isReserve());
        return busRepository.save(bus);
    }

    @ApiOperation(value="delete bus by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        busRepository.deleteById(id);
    }

}
