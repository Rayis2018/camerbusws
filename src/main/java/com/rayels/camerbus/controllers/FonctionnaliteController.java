package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Fonctionnalite;
import com.rayels.camerbus.entities.Pays;
import com.rayels.camerbus.repositories.FonctionnaliteRepository;
import com.rayels.camerbus.repositories.PaysRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fonctionnalite")
public class FonctionnaliteController {

    @Autowired
    FonctionnaliteRepository fonctionnaliteRepository;

    @GetMapping("/find/all")
    public Iterable<Fonctionnalite> getAllFonctionnalite() {
        return fonctionnaliteRepository.findAll();
    }

    @GetMapping("/find/{id}")
    public Fonctionnalite getFonctionnaliteById(@PathVariable Long id){
        return fonctionnaliteRepository.findById(id).orElse(null);
    }

    @GetMapping("/find/{libelle}")
    public Fonctionnalite getFonctionnaliteByLIbelle(@PathVariable String libelle){
        return fonctionnaliteRepository.findByLibelle(libelle);
    }
}
