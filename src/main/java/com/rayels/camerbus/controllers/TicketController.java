package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Ticket;
import com.rayels.camerbus.repositories.*;
import com.rayels.camerbus.requests.TicketRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/ticket")
public class TicketController {

    @Autowired
    TicketRepository ticketRepository;
    @Autowired
    BonAchatRepository bonAchatRepository;
    @Autowired
    ModePaiementRepository modePaiementRepository;
    @Autowired
    PointRepository pointRepository;
    @Autowired
    UtilisateurRepository utilisateurRepository;
    @Autowired
    VoyageRepository voyageRepository;

    @GetMapping("/find/all")
    public Iterable<Ticket> getAllTicket() {
        return ticketRepository.findAll();
    }

    @ApiOperation(value="find Ticket by id",response= Ticket.class)
    @GetMapping("/find/{id}")
    public Ticket getTicketById(@PathVariable Long id){
        return ticketRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find Ticket by numero",response= Ticket.class)
    @GetMapping("/find/{numero}")
    public Ticket getColisByNumero(@PathVariable int numero){
        return ticketRepository.findByNumero(numero);
    }

    @ApiOperation(value="add new Ticket",response= Ticket.class)
    @PostMapping("/add")
    public Ticket add(@Valid @RequestBody TicketRequest ticketRequest){
        Ticket ticket = new Ticket();
        ticket.setAnnule(false);
        ticket.setBonAchat(bonAchatRepository.findById(ticketRequest.getIdBonAchat()).orElse(null));
        ticket.setCloture(false);
        ticket.setCodePaiement(ticketRequest.getCodePaiement());
        ticket.setCodeQR(ticketRequest.getCodeQR());
//        ticket.setDateAnnulation(ticketRequest.getAnnulation());
        ticket.setAnnule(false);
//        ticket.setDateArrivee(ticketRequest.getDateArrivee());
        ticket.setDateCreation(new Date());
//        ticket.setDateDepart(ticketRequest.getDateDepart());
//        ticket.setDateExpiration(ticketRequest.getDateExpiration());
//        ticket.setDateRepro(ticketRequest.getDateRepro());
//        ticket.setDateReservation(ticketRequest.getDateReservation());
        ticket.setFrais(ticketRequest.getFrais());
        ticket.setModePaiement(modePaiementRepository.findById(ticketRequest.getIdModePaiement()).orElse(null));
        ticket.setNomClient(ticketRequest.getNomClient());
        ticket.setNote(ticketRequest.getNote());
        ticket.setNote2(ticketRequest.getNote2());
        ticket.setNumero(ticketRequest.getNumero());
        ticket.setPoint(pointRepository.findById(ticketRequest.getIdPoint()).orElse(null));
        ticket.setPrix(ticketRequest.getPrix());
        ticket.setReprogramme(false);
        ticket.setReserve(false);
        ticket.setUtilisateur(utilisateurRepository.findById(ticketRequest.getIdUtilisateur()).orElse(null));
        ticket.setUtilise(false);
        ticket.setVoyage(voyageRepository.findById(ticketRequest.getIdVoyage()).orElse(null));
        return ticketRepository.save(ticket);
    }

    @ApiOperation(value="update Ticket by id",response= Ticket.class)
    @PutMapping("/update/{id}")
    public Ticket add(@PathVariable long id, @Valid @RequestBody TicketRequest ticketRequest){
        Ticket ticket = ticketRepository.findById(id).orElse(null);
        ticket.setAnnule(false);
        ticket.setBonAchat(bonAchatRepository.findById(ticketRequest.getIdBonAchat()).orElse(null));
        ticket.setCloture(false);
        ticket.setCodePaiement(ticketRequest.getCodePaiement());
        ticket.setCodeQR(ticketRequest.getCodeQR());
//        ticket.setDateAnnulation(ticketRequest.getAnnulation());
        ticket.setAnnule(false);
//        ticket.setDateArrivee(ticketRequest.getDateArrivee());
//        ticket.setDateDepart(ticketRequest.getDateDepart());
//        ticket.setDateExpiration(ticketRequest.getDateExpiration());
//        ticket.setDateRepro(ticketRequest.getDateRepro());
//        ticket.setDateReservation(ticketRequest.getDateReservation());
        ticket.setFrais(ticketRequest.getFrais());
        ticket.setModePaiement(modePaiementRepository.findById(ticketRequest.getIdModePaiement()).orElse(null));
        ticket.setNomClient(ticketRequest.getNomClient());
        ticket.setNote(ticketRequest.getNote());
        ticket.setNote2(ticketRequest.getNote2());
        ticket.setNumero(ticketRequest.getNumero());
        ticket.setPoint(pointRepository.findById(ticketRequest.getIdPoint()).orElse(null));
        ticket.setPrix(ticketRequest.getPrix());
        ticket.setReprogramme(false);
        ticket.setReserve(false);
        ticket.setUtilisateur(utilisateurRepository.findById(ticketRequest.getIdUtilisateur()).orElse(null));
        ticket.setUtilise(false);
        ticket.setVoyage(voyageRepository.findById(ticketRequest.getIdVoyage()).orElse(null));
        return ticketRepository.save(ticket);
    }

    @ApiOperation(value="delete Ticket by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        ticketRepository.deleteById(id);
    }
}
