package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.TypeCompte;
import com.rayels.camerbus.repositories.TypeCompteRepository;
import com.rayels.camerbus.requests.TypeCompteRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/typecompte")
public class TypeCompteController {

    @Autowired
    TypeCompteRepository typeCompteRepository;

    @GetMapping("/find/all")
    public Iterable<TypeCompte> getAllTypeCompte() {
        return typeCompteRepository.findAll();
    }

    @ApiOperation(value="find Type Compte by id",response= TypeCompte.class)
    @GetMapping("/find/{id}")
    public TypeCompte getTypeCompteById(@PathVariable Long id){
        return typeCompteRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find Type Compte by libelle",response= TypeCompte.class)
    @GetMapping("/find/{libelle}")
    public TypeCompte getTypeCompteByLibelle(@PathVariable String libelle){
        return typeCompteRepository.findByLibelle(libelle);
    }

    @ApiOperation(value="add Type Compte",response= TypeCompte.class)
    @PostMapping("/add")
    public TypeCompte add(@Valid @RequestBody TypeCompteRequest typeCompteRequest){
        TypeCompte typeCompte = new TypeCompte();
        typeCompte.setDateCreation(new Date());
        typeCompte.setLibelle(typeCompteRequest.getLibelle());
        return typeCompteRepository.save(typeCompte);
    }

    @ApiOperation(value="update Type Compte by id",response= TypeCompte.class)
    @PutMapping("/update/{id}")
    public TypeCompte add(@PathVariable long id, @Valid @RequestBody TypeCompteRequest typeCompteRequest){
        TypeCompte typeCompte = typeCompteRepository.findById(id).orElse(null);
        typeCompte.setLibelle(typeCompteRequest.getLibelle());
        return typeCompteRepository.save(typeCompte);
    }

    @ApiOperation(value="delete Type Compte by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        typeCompteRepository.deleteById(id);
    }
}
