package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.BonAchat;
import com.rayels.camerbus.repositories.BonAchatRepository;
import com.rayels.camerbus.repositories.TicketRepository;
import com.rayels.camerbus.requests.BonAchatRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/bonachat")
public class BonAchatController {

    @Autowired
    BonAchatRepository bonAchatRepository;

    @Autowired
    TicketRepository ticketRepository;

    @GetMapping("/find/all")
    public Iterable<BonAchat> getAllBonAchat() {
        return bonAchatRepository.findAll();
    }

    @ApiOperation(value="find bonAchat by id",response= BonAchat.class)
    @GetMapping("/find/{id}")
    public BonAchat getBonAchatById(@PathVariable Long id){
        return bonAchatRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find bonAchat by numero",response= BonAchat.class)
    @GetMapping("/find/{numero}")
    public BonAchat getBonAchatByNumero(@PathVariable String numero){
        return bonAchatRepository.findBonAchatByNumero(numero);
    }

    @ApiOperation(value="add new bonAchat ")
    @PostMapping("/add")
    public BonAchat add(@Valid @RequestBody BonAchatRequest bonAchatRequest){
        BonAchat bonAchat = new BonAchat();
        bonAchat.setAdresse(bonAchatRequest.getAdresse());
        bonAchat.setDateCreation(new Date());
        bonAchat.setDateValid(bonAchatRequest.getDateValid());
        bonAchat.setMontant(bonAchatRequest.getMontant());
        bonAchat.setNomClient(bonAchatRequest.getNomClient());
        bonAchat.setNumero(bonAchatRequest.getNumero());
        bonAchat.setTelephone(bonAchatRequest.getTelephone());
        bonAchat.setTicket(ticketRepository.findById(bonAchatRequest.getIdTicket()).orElse(null));
        bonAchat.setUtilise(false);
        return bonAchatRepository.save(bonAchat);
    }

    @ApiOperation(value="update bonAchat by id",response= BonAchat.class)
    @PutMapping("/update/{id}")
    public BonAchat update(@PathVariable int id, @Valid @RequestBody BonAchatRequest bonAchatRequest){
        BonAchat bonAchat = bonAchatRepository.findBonAchatById(id);
        bonAchat.setAdresse(bonAchatRequest.getAdresse());
        bonAchat.setDateValid(bonAchatRequest.getDateValid());
        bonAchat.setMontant(bonAchatRequest.getMontant());
        bonAchat.setNomClient(bonAchatRequest.getNomClient());
        bonAchat.setNumero(bonAchatRequest.getNumero());
        bonAchat.setTelephone(bonAchatRequest.getTelephone());
        bonAchat.setTicket(ticketRepository.findById(bonAchatRequest.getIdTicket()).orElse(null));
        bonAchat.setUtilise(false);
        return bonAchatRepository.save(bonAchat);
    }

    @ApiOperation(value="delete bonAchat by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        bonAchatRepository.deleteById(id);
    }
}
