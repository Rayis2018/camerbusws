package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Ticket;
import com.rayels.camerbus.entities.Voyage;
import com.rayels.camerbus.repositories.*;
import com.rayels.camerbus.requests.VoyageRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/voyage")
public class VoyageController {

    @Autowired
    VoyageRepository voyageRepository;
    @Autowired
    AgenceRepository agenceRepository;
    @Autowired
    TicketRepository ticketRepository;
    @Autowired
    BusRepository busRepository;
    @Autowired
    VilleRepository villeRepository;

    @GetMapping("/find/all")
    public Iterable<Voyage> getAllVoyage() {
        return voyageRepository.findAll();
    }

    @ApiOperation(value="find Voyage by it's id",response= Voyage.class)
    @GetMapping("/find/{id}")
    public Voyage getVoyageById(@PathVariable Long id){
        return voyageRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find Voyage by idTicket",response= Voyage.class)
    @GetMapping("/find/{idTicket}")
    public Voyage getVoyageByTicket(@PathVariable Long idTicket){
        Ticket ticket = ticketRepository.findById(idTicket).orElse(null);
        return voyageRepository.findByTicket(ticket);
    }

    @ApiOperation(value="add new Voyage",response= Voyage.class)
    @PostMapping("/add")
    public Voyage add(@Valid @RequestBody VoyageRequest voyageRequest){
        Voyage voyage = new Voyage();
        voyage.setAgence(agenceRepository.findById(voyageRequest.getIdAgence()).orElse(null));
        voyage.setBus(busRepository.findById(voyageRequest.getIdBus()).orElse(null));
        voyage.setDateArrivee(voyageRequest.getDateArrivee());
        voyage.setDateCreation(new Date());
        voyage.setDateDepart(voyageRequest.getDateDepart());
        voyage.setNote(voyageRequest.getNote());
        voyage.setPrix(voyageRequest.getPrix());
        voyage.setVilleArrivee(villeRepository.findById(voyageRequest.getIdVilleArrivee()).orElse(null));
        voyage.setVilleDepart(villeRepository.findById(voyageRequest.getIdVilleDepart()).orElse(null));
        return voyageRepository.save(voyage);
    }

    @ApiOperation(value="update Voyage by id",response= Voyage.class)
    @PutMapping("/update/{id}")
    public Voyage update(@PathVariable long id, @Valid @RequestBody VoyageRequest voyageRequest){
        Voyage voyage = voyageRepository.findById(id).orElse(null);
        voyage.setAgence(agenceRepository.findById(voyageRequest.getIdAgence()).orElse(null));
        voyage.setBus(busRepository.findById(voyageRequest.getIdBus()).orElse(null));
        voyage.setDateArrivee(voyageRequest.getDateArrivee());
        voyage.setDateDepart(voyageRequest.getDateDepart());
        voyage.setNote(voyageRequest.getNote());
        voyage.setPrix(voyageRequest.getPrix());
        voyage.setVilleArrivee(villeRepository.findById(voyageRequest.getIdVilleArrivee()).orElse(null));
        voyage.setVilleDepart(villeRepository.findById(voyageRequest.getIdVilleDepart()).orElse(null));
        return voyageRepository.save(voyage);
    }

    @ApiOperation(value="delete Voyage by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        voyageRepository.deleteById(id);
    }
}
