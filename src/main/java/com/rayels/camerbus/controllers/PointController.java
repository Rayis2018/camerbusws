package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Point;
import com.rayels.camerbus.entities.Ticket;
import com.rayels.camerbus.repositories.PointRepository;
import com.rayels.camerbus.repositories.TicketRepository;
import com.rayels.camerbus.requests.PointRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/point")
public class PointController {

    @Autowired
    PointRepository pointRepository;
    @Autowired
    TicketRepository ticketRepository;

    @GetMapping("/find/all")
    public Iterable<Point> getAllPoint() {
        return pointRepository.findAll();
    }

    @ApiOperation(value="find Point by it's id",response= Point.class)
    @GetMapping("/find/{id}")
    public Point getPointById(@PathVariable Long id){
        return pointRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find Point by idTicket",response= Point.class)
    @GetMapping("/find/{idTicket}")
    public Point getByIdTicket(@PathVariable Long idTicket){
        Ticket ticket  = ticketRepository.findById(idTicket).orElse(null);
        return pointRepository.findByTicket(ticket);
    }

    @ApiOperation(value="add new Point",response= Point.class)
    @PostMapping("/add")
    public Point add(@Valid @RequestBody PointRequest pointRequest){
        Point point = new Point();
        point.setDateCreation(new Date());
        point.setNombre(pointRequest.getNombre());
        point.setStatut(false);
        point.setTicket(ticketRepository.findById(pointRequest.getIdTicket()).orElse(null));
        return pointRepository.save(point);
    }

    @ApiOperation(value="update Point by id",response= Point.class)
    @PutMapping("/update/{id}")
    public Point update(@PathVariable long id, @Valid @RequestBody PointRequest pointRequest){
        Point point = pointRepository.findById(id).orElse(null);
        point.setNombre(pointRequest.getNombre());
//        point.setStatut(false);
        point.setTicket(ticketRepository.findById(pointRequest.getIdTicket()).orElse(null));
        return pointRepository.save(point);
    }

    @ApiOperation(value="delete Point by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        pointRepository.deleteById(id);
    }

}
