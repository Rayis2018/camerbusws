package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Utilisateur;
import com.rayels.camerbus.repositories.AgenceRepository;
import com.rayels.camerbus.repositories.CompteRepository;
import com.rayels.camerbus.repositories.PosteRepository;
import com.rayels.camerbus.repositories.UtilisateurRepository;
import com.rayels.camerbus.requests.UtilisateurRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/utilisateur")
public class UtilisateurController {

    @Autowired
    UtilisateurRepository utilisateurRepository;
    @Autowired
    AgenceRepository agenceRepository;
    @Autowired
    CompteRepository compteRepository;
    @Autowired
    PosteRepository posteRepository;

    @GetMapping("/find/all")
    public Iterable<Utilisateur> getAllUtilisateur() {
        return utilisateurRepository.findAll();
    }

    @ApiOperation(value="find User by id",response= Utilisateur.class)
    @GetMapping("/find/{id}")
    public Utilisateur getUtilisateurById(@PathVariable Long id){
        return utilisateurRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find User by name",response= Utilisateur.class)
    @GetMapping("/find/{nom}")
    public Utilisateur getUtilisateurByNom(@PathVariable String nom){
        return utilisateurRepository.findByNom(nom);
    }

    @ApiOperation(value="add new User",response= Utilisateur.class)
    @PostMapping("/add")
    public Utilisateur add(@Valid @RequestBody UtilisateurRequest utilisateurRequest){
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setAdresse(utilisateurRequest.getAdresse());
        utilisateur.setAgence(agenceRepository.findById(utilisateurRequest.getIdAgence()).orElse(null));
        utilisateur.setCodePostal(utilisateurRequest.getCodePostal());
        utilisateur.setCompte(compteRepository.findById(utilisateurRequest.getIdCompte()).orElse(null));
        utilisateur.setDateCreation(new Date());
        utilisateur.setEmail(utilisateurRequest.getEmail());
        utilisateur.setFax(utilisateurRequest.getFax());
        utilisateur.setNom(utilisateurRequest.getNom());
        utilisateur.setPhoto(utilisateurRequest.getPhoto());
        utilisateur.setPoste(posteRepository.findById(utilisateurRequest.getIdPoste()).orElse(null));
        utilisateur.setPrenom(utilisateurRequest.getPrenom());
        utilisateur.setSexe(utilisateurRequest.getSexe());
        utilisateur.setTelephone(utilisateurRequest.getTelephone());
        return utilisateurRepository.save(utilisateur);
    }

    @ApiOperation(value="update User by id",response= Utilisateur.class)
    @PutMapping("/update/{id}")
    public Utilisateur update(@PathVariable long id, @Valid @RequestBody UtilisateurRequest utilisateurRequest){
        Utilisateur utilisateur = utilisateurRepository.findById(id).orElse(null);
        utilisateur.setAdresse(utilisateurRequest.getAdresse());
        utilisateur.setAgence(agenceRepository.findById(utilisateurRequest.getIdAgence()).orElse(null));
        utilisateur.setCodePostal(utilisateurRequest.getCodePostal());
        utilisateur.setCompte(compteRepository.findById(utilisateurRequest.getIdCompte()).orElse(null));
        utilisateur.setEmail(utilisateurRequest.getEmail());
        utilisateur.setFax(utilisateurRequest.getFax());
        utilisateur.setNom(utilisateurRequest.getNom());
        utilisateur.setPhoto(utilisateurRequest.getPhoto());
        utilisateur.setPoste(posteRepository.findById(utilisateurRequest.getIdPoste()).orElse(null));
        utilisateur.setPrenom(utilisateurRequest.getPrenom());
        utilisateur.setSexe(utilisateurRequest.getSexe());
        utilisateur.setTelephone(utilisateurRequest.getTelephone());
        return utilisateurRepository.save(utilisateur);
    }

    @ApiOperation(value="delete User by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        utilisateurRepository.deleteById(id);
    }

}
