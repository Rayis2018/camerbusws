package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Ville;
import com.rayels.camerbus.repositories.VilleRepository;
import com.rayels.camerbus.requests.VilleRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/ville")
public class VilleController {

    @Autowired
    VilleRepository villeRepository;

    @GetMapping("/find/all")
    public Iterable<Ville> getAllVille() {
        return villeRepository.findAll();
    }

    @ApiOperation(value="find Ville by id",response= Ville.class)
    @GetMapping("/find/{id}")
    public Ville getVilleById(@PathVariable Long id){
        return villeRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find Ville by nom",response= Ville.class)
    @GetMapping("/find/{nom}")
    public Ville getVilleByNom(@PathVariable String nom){
        return villeRepository.findByNom(nom);
    }

    @ApiOperation(value="add new Ville",response= Ville.class)
    @PostMapping("/add")
    public Ville add(@Valid @RequestBody VilleRequest villeRequest){
        Ville ville = new Ville();
        ville.setNom(villeRequest.getNom());
        ville.setNote(villeRequest.getNote());
        return villeRepository.save(ville);
    }

    @ApiOperation(value="update Ville by id",response= Ville.class)
    @PutMapping("/update/{id}")
    public Ville add(@PathVariable long id, @Valid @RequestBody VilleRequest villeRequest){
        Ville ville = villeRepository.findById(id).orElse(null);
        ville.setNom(villeRequest.getNom());
        ville.setNote(villeRequest.getNote());
        return villeRepository.save(ville);
    }

    @ApiOperation(value="delete Ville by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        villeRepository.deleteById(id);
    }

}
