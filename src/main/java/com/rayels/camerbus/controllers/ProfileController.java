package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Profile;
import com.rayels.camerbus.repositories.PermissionRepository;
import com.rayels.camerbus.repositories.ProfileRepository;
import com.rayels.camerbus.requests.ProfileRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/profile")
public class ProfileController {

    @Autowired
    ProfileRepository profileRepository;
    @Autowired
    PermissionRepository permissionRepository;

    @GetMapping("/find/all")
    public Iterable<Profile> getAllProfile() {
        return profileRepository.findAll();
    }

    @ApiOperation(value="find Profile by id",response= Profile.class)
    @GetMapping("/find/{id}")
    public Profile getProfileById(@PathVariable Long id){
        return profileRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find Profile by nom",response= Profile.class)
    @GetMapping("/find/{nom}")
    public Profile getProfileByNom(@PathVariable String nom){
        return profileRepository.findByNom(nom);
    }

    @ApiOperation(value="add new Profile",response= Profile.class)
    @PostMapping("/add")
    public  Profile add(@Valid @RequestBody ProfileRequest profileRequest){
        Profile profile = new Profile();
        profile.setDateCreation(new Date());
        profile.setNom(profileRequest.getNom());
        profile.setPermission(permissionRepository.findById(profileRequest.getIdPermission()).orElse(null));
        return profileRepository.save(profile);
    }

    @ApiOperation(value="update Profile by id",response= Profile.class)
    @PutMapping("/update/{id}")
    public  Profile update(@PathVariable long id, @Valid @RequestBody ProfileRequest profileRequest){
        Profile profile = profileRepository.findById(id).orElse(null);
        profile.setNom(profileRequest.getNom());
        profile.setPermission(permissionRepository.findById(profileRequest.getIdPermission()).orElse(null));
        return profileRepository.save(profile);
    }

    @ApiOperation(value="delte Profile by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        profileRepository.deleteById(id);
    }


}
