package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Entreprise;
import com.rayels.camerbus.repositories.EntrepriseRepository;
import com.rayels.camerbus.requests.EntrepriseRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/entreprise")
public class EntrepriseController {

    @Autowired
    EntrepriseRepository entrepriseRepository;

    @GetMapping("/find/all")
    public Iterable<Entreprise> getAllEntreprise() {
        return entrepriseRepository.findAll();
    }


    @ApiOperation(value="find entreprise by id",response= Entreprise.class)
    @GetMapping("/find/{id}")
    public Entreprise getEntrepriseById(@PathVariable Long id){
        return entrepriseRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find entreprise by it's nomCOmmercial",response= Entreprise.class)
    @GetMapping("/find/{nomCommercial}")
    public Entreprise getBonAchatByNumero(@PathVariable String nomCommercial){
        return entrepriseRepository.findByNomCommercial(nomCommercial);
    }

    @ApiOperation(value="add new entreprise",response= Entreprise.class)
    @PostMapping("/add")
    public Entreprise add(@Valid @RequestBody EntrepriseRequest entrepriseRequest){
        Entreprise entreprise = new Entreprise();
        entreprise.setActivite(entrepriseRequest.getActivite());
        entreprise.setAdresse(entrepriseRequest.getAdresse());
        entreprise.setBranche(entrepriseRequest.getBranche());
        entreprise.setCapital(entrepriseRequest.getCapital());
        entreprise.setDateCreation(new Date());
        entreprise.setCodePostal(entrepriseRequest.getCodePostal());
        entreprise.setEmail(entrepriseRequest.getEmail());
        entreprise.setLogo(entrepriseRequest.getLogo());
        entreprise.setNomCommercial(entrepriseRequest.getNomCommercial());
        entreprise.setNote(entrepriseRequest.getNote());
        entreprise.setRegime(entrepriseRequest.getRegime());
        entreprise.setRegistre(entrepriseRequest.getRegistre());
        entreprise.setSiteweb(entrepriseRequest.getSiteweb());
        entreprise.setTelephone(entrepriseRequest.getTelephone());
        return entrepriseRepository.save(entreprise);
    }

    @ApiOperation(value="update entreprise by id",response= Entreprise.class)
    @PutMapping("/update/{id}")
    public Entreprise add(@PathVariable long id, @Valid @RequestBody EntrepriseRequest entrepriseRequest){
        Entreprise entreprise = entrepriseRepository.findById(id).orElse(null);
        entreprise.setActivite(entrepriseRequest.getActivite());
        entreprise.setAdresse(entrepriseRequest.getAdresse());
        entreprise.setBranche(entrepriseRequest.getBranche());
        entreprise.setCapital(entrepriseRequest.getCapital());
        entreprise.setCodePostal(entrepriseRequest.getCodePostal());
        entreprise.setEmail(entrepriseRequest.getEmail());
        entreprise.setLogo(entrepriseRequest.getLogo());
        entreprise.setNomCommercial(entrepriseRequest.getNomCommercial());
        entreprise.setNote(entrepriseRequest.getNote());
        entreprise.setRegime(entrepriseRequest.getRegime());
        entreprise.setRegistre(entrepriseRequest.getRegistre());
        entreprise.setSiteweb(entrepriseRequest.getSiteweb());
        entreprise.setTelephone(entrepriseRequest.getTelephone());
        return entrepriseRepository.save(entreprise);
    }

    @ApiOperation(value="delete entreprise by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        entrepriseRepository.deleteById(id);
    }

}
