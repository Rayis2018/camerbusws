package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Colis;
import com.rayels.camerbus.repositories.ColisRepository;
import com.rayels.camerbus.repositories.TicketRepository;
import com.rayels.camerbus.requests.ColisRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/colis")
public class ColisController {

    @Autowired
    ColisRepository colisRepository;
    TicketRepository ticketRepository;

    @GetMapping("/find/all")
    public Iterable<Colis> getAllColis() {
        return colisRepository.findAll();
    }

    @ApiOperation(value="find colis by id",response= Colis.class)
    @GetMapping("/find/{id}")
    public Colis getColisById(@PathVariable Long id){
        return colisRepository.findById(id).orElse(null);
    }


    @ApiOperation(value="find colis by number",response= Colis.class)
    @GetMapping("/find/{numero}")
    public Colis getColisByNumero(@PathVariable String numero){
        return colisRepository.findByNumero(numero);
    }

    @ApiOperation(value="add new colis",response= Colis.class)
    @PostMapping("/add")
    public Colis add(@Valid @RequestBody ColisRequest colisRequest){
        Colis colis = new Colis();
        colis.setCodeBar(colisRequest.getCodeBar());
        colis.setCouleur(colisRequest.getCouleur());
        colis.setDateCreation(new Date());
        colis.setHauteur(colisRequest.getHauteur());
        colis.setLargeur(colisRequest.getLargeur());
        colis.setLibelle(colisRequest.getLibelle());
        colis.setLongueur(colisRequest.getLongueur());
        colis.setNumero(colisRequest.getNumero());
        colis.setPoids(colisRequest.getPoids());
        colis.setPrixFret(colisRequest.getPrixFret());
        colis.setTicket(ticketRepository.findById(colisRequest.getIdTicket()).orElse(null));
        return colisRepository.save(colis);
    }

    @ApiOperation(value="update colis by id",response= Colis.class)
    @PutMapping("/update/{id}")
    public Colis add(@PathVariable long id, @Valid @RequestBody ColisRequest colisRequest){
        Colis colis = colisRepository.findById(id).orElse(null);
        colis.setCodeBar(colisRequest.getCodeBar());
        colis.setCouleur(colisRequest.getCouleur());
        colis.setHauteur(colisRequest.getHauteur());
        colis.setLargeur(colisRequest.getLargeur());
        colis.setLibelle(colisRequest.getLibelle());
        colis.setLongueur(colisRequest.getLongueur());
        colis.setNumero(colisRequest.getNumero());
        colis.setPoids(colisRequest.getPoids());
        colis.setPrixFret(colisRequest.getPrixFret());
        colis.setTicket(ticketRepository.findById(colisRequest.getIdTicket()).orElse(null));
        return colisRepository.save(colis);
    }

    @ApiOperation(value="delete colis by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        colisRepository.deleteById(id);
    }

}
