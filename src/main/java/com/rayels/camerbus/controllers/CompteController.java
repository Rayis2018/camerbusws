package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Compte;
import com.rayels.camerbus.repositories.CompteRepository;
import com.rayels.camerbus.repositories.ProfileRepository;
import com.rayels.camerbus.repositories.TypeCompteRepository;
import com.rayels.camerbus.repositories.UtilisateurRepository;
import com.rayels.camerbus.requests.CompteRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/compte")
public class CompteController {

    @Autowired
    CompteRepository compteRepository;
    @Autowired
    ProfileRepository profileRepository;
    @Autowired
    TypeCompteRepository typeCompteRepository;
    @Autowired
    UtilisateurRepository utilisateurRepository;

    @GetMapping("/find/all")
    public Iterable<Compte> getAllBonAchat() {
        return compteRepository.findAll();
    }

    @ApiOperation(value="find account by id",response= Compte.class)
    @GetMapping("/find/{id}")
    public Compte getCompteById(@PathVariable Long id){
        return compteRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find account by it's number",response= Compte.class)
    @GetMapping("/find/{numero}")
    public Compte getBonAchatByNumero(@PathVariable int numero){
        return compteRepository.findByNumero(numero);
    }

    @ApiOperation(value="add new account",response= Compte.class)
    @PostMapping("/add")
    public Compte add(@Valid @RequestBody CompteRequest compteRequest){
        Compte compte = new Compte();
        compte.setDateCreation(new Date());
        compte.setNbrePoints(compteRequest.getNbrePoints());
        compte.setNumero(compteRequest.getNumero());
        compte.setProfile(profileRepository.findById(compteRequest.getIdProfile()).orElse(null));
        compte.setSolde(compteRequest.getSolde());
        compte.setTypeCompte(typeCompteRepository.findById(compteRequest.getIdTypeCompte()).orElse(null));
        compte.setUtilisateur(utilisateurRepository.findById(compteRequest.getIdUtilisateur()).orElse(null));
        return compteRepository.save(compte);
    }

    @ApiOperation(value="update account by id",response= Compte.class)
    @PutMapping("/update/{id}")
    public Compte update(@PathVariable long id, @Valid @RequestBody CompteRequest compteRequest){
        Compte compte = compteRepository.findById(id).orElse(null);
        compte.setNbrePoints(compteRequest.getNbrePoints());
        compte.setNumero(compteRequest.getNumero());
        compte.setDateModif(new Date());
        compte.setProfile(profileRepository.findById(compteRequest.getIdProfile()).orElse(null));
        compte.setSolde(compteRequest.getSolde());
        compte.setTypeCompte(typeCompteRepository.findById(compteRequest.getIdTypeCompte()).orElse(null));
        compte.setUtilisateur(utilisateurRepository.findById(compteRequest.getIdUtilisateur()).orElse(null));
        return compteRepository.save(compte);
    }

    @ApiOperation(value="delete account by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        compteRepository.deleteById(id);
    }

}
