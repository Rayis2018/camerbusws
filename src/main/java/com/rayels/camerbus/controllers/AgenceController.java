package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Agence;
import com.rayels.camerbus.repositories.AgenceRepository;
import com.rayels.camerbus.repositories.EntrepriseRepository;
import com.rayels.camerbus.repositories.VilleRepository;
import com.rayels.camerbus.requests.AgenceRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/agence")
public class AgenceController {

    @Autowired
    AgenceRepository agenceRepository;
    @Autowired
    VilleRepository villeRepository;
    @Autowired
    EntrepriseRepository entrepriseRepository;

    @ApiOperation(value="find all agence")
    @GetMapping("/find/all")
    public Iterable<Agence> getAllAgencies() {
        return agenceRepository.findAll();
    }

    @ApiOperation(value="find agence by id",response=Agence.class)
    @GetMapping("/find/{id}")
    public Agence getAgenceById(@PathVariable Long id){
        return agenceRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find agence by code",response=Agence.class)
    @GetMapping("/find/{code}")
    public Agence getAgenceByCode(@PathVariable String code){
        return agenceRepository.findAgenceByCode(code);
    }

    @ApiOperation(value="add new agence",response=Agence.class)
    @PostMapping("/add")
    public Agence addAgence(@Valid @RequestBody AgenceRequest agenceRequest){
        Agence agence  = new Agence();
        agence.setAdresse(agenceRequest.getAdresse());
        agence.setCode(agenceRequest.getCode());
        agence.setCodePostal(agenceRequest.getCodePostal());
        agence.setDateCreation(new Date());
        agence.setEmail(agenceRequest.getEmail());
        agence.setFax(agenceRequest.getFax());
        agence.setLatitude(agenceRequest.getLatitude());
        agence.setNomCommercial(agenceRequest.getNomCommercial());
        agence.setLongitude(agence.getLongitude());
        agence.setAltitude(agenceRequest.getAltitude());
        agence.setTelephone(agenceRequest.getTelephone());
        agence.setSiteweb(agenceRequest.getWebside());
        agence.setEntreprise(entrepriseRepository.findById(agenceRequest.getIdEntreprise()).orElse(null));
        agence.setVille(villeRepository.findById(agenceRequest.getIdVille()).orElse(null));
        return agenceRepository.save(agence);
    }

    @ApiOperation(value="update agency",response=Agence.class)
    @PutMapping("/update/{id}")
    public Agence updateAgence(@PathVariable long id, @Valid @RequestBody AgenceRequest agenceRequest){
        Agence agence = agenceRepository.findById(id).orElse(null);
        agence.setAdresse(agenceRequest.getAdresse());
        agence.setCode(agenceRequest.getCode());
        agence.setCodePostal(agenceRequest.getCodePostal());
        agence.setDateCreation(new Date());
        agence.setEmail(agenceRequest.getEmail());
        agence.setFax(agenceRequest.getFax());
        agence.setLatitude(agenceRequest.getLatitude());
        agence.setNomCommercial(agenceRequest.getNomCommercial());
        agence.setLongitude(agence.getLongitude());
        agence.setAltitude(agenceRequest.getAltitude());
        agence.setTelephone(agenceRequest.getTelephone());
        agence.setSiteweb(agenceRequest.getWebside());
        agence.setEntreprise(entrepriseRepository.findById(agenceRequest.getIdEntreprise()).orElse(null));
        agence.setVille(villeRepository.findById(agenceRequest.getIdVille()).orElse(null));
        return agenceRepository.save(agence);
    }

    @ApiOperation(value = "Delete agency")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        agenceRepository.deleteById(id);
    }
}
