package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Pays;
import com.rayels.camerbus.repositories.PaysRepository;
import com.rayels.camerbus.requests.PaysRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/pays")
public class PaysController {

    @Autowired
    PaysRepository paysRepository;

    @GetMapping("/find/all")
    public Iterable<Pays> getAllPays() {
        return paysRepository.findAll();
    }

    @ApiOperation(value="find Pays by id",response= Pays.class)
    @GetMapping("/find/{id}")
    public Pays getPaysById(@PathVariable Long id){
        return paysRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find Pays by nom",response= Pays.class)
    @GetMapping("/find/{nom}")
    public Pays getPaysByNom(@PathVariable String nom){
        return paysRepository.findByNom(nom);
    }

    @ApiOperation(value="add new Pays",response= Pays.class)
    @PostMapping("/add")
    public Pays add(@Valid @RequestBody PaysRequest paysRequest){
        Pays pays = new Pays();
        pays.setNom(paysRequest.getNom());
        pays.setDateCreation(new Date());
        return paysRepository.save(pays);
    }

    @ApiOperation(value="update Pays by id",response= Pays.class)
    @PutMapping("/update/{id}")
    public Pays update(@PathVariable long id, @Valid @RequestBody PaysRequest paysRequest){
        Pays pays =paysRepository.findById(id).orElse(null);
        pays.setNom(paysRequest.getNom());
        return paysRepository.save(pays);
    }

    @ApiOperation(value="delete pays by id",response= Pays.class)
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        paysRepository.deleteById(id);
    }

}
