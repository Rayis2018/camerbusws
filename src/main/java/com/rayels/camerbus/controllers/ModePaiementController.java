package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.ModePaiement;
import com.rayels.camerbus.repositories.ModePaiementRepository;
import com.rayels.camerbus.requests.ModePaiementRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/modepaiement")
public class ModePaiementController {

    @Autowired
    ModePaiementRepository modePaiementRepository;

    @GetMapping("/find/all")
    public Iterable<ModePaiement> getAllModePaiement() {
        return modePaiementRepository.findAll();
    }

    @ApiOperation(value="find modePaiement by it's id",response= ModePaiement.class)
    @GetMapping("/find/{id}")
    public ModePaiement getModePaiementById(@PathVariable Long id){
        return modePaiementRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find modePaiement by it's libelle",response= ModePaiement.class)
    @GetMapping("/find/{libelle}")
    public ModePaiement getModePaiementByLibelle(@PathVariable String libelle){
        return modePaiementRepository.findByLibelle(libelle);
    }

    @ApiOperation(value="add modePaiement",response= ModePaiement.class)
    @PostMapping("/add")
    public ModePaiement add(@Valid @RequestBody ModePaiementRequest modePaiementRequest){
        ModePaiement modePaiement = new ModePaiement();
        modePaiement.setLibelle(modePaiementRequest.getLibelle());
        modePaiement.setDateCreation(new Date());
        return modePaiementRepository.save(modePaiement);
    }

    @ApiOperation(value="update modePaiement by id",response= ModePaiement.class)
    @PutMapping("/update/{id}")
    public ModePaiement add(@PathVariable long id, @Valid @RequestBody ModePaiementRequest modePaiementRequest){
        ModePaiement modePaiement = modePaiementRepository.findById(id).orElse(null);
        modePaiement.setLibelle(modePaiementRequest.getLibelle());
        return modePaiementRepository.save(modePaiement);
    }

    @ApiOperation(value="delete modePaiement by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        modePaiementRepository.deleteById(id);
    }
}
