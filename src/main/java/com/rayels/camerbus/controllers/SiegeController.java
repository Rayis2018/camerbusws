package com.rayels.camerbus.controllers;

import com.rayels.camerbus.entities.Siege;
import com.rayels.camerbus.repositories.BusRepository;
import com.rayels.camerbus.repositories.SiegeRepository;
import com.rayels.camerbus.requests.SiegeRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/siege")
public class SiegeController {

    @Autowired
    SiegeRepository siegeRepository;
    @Autowired
    BusRepository busRepository;

    @GetMapping("/find/all")
    public Iterable<Siege> getAllSiege() {
        return siegeRepository.findAll();
    }

    @ApiOperation(value="find Siege by id",response= Siege.class)
    @GetMapping("/find/{id}")
    public Siege getSiegeById(@PathVariable Long id){
        return siegeRepository.findById(id).orElse(null);
    }

    @ApiOperation(value="find Siege by numero",response= Siege.class)
    @GetMapping("/find/{numero}")
    public Siege getSiegeByNumero(@PathVariable String numero){
        return siegeRepository.findByNumero(numero);
    }

    @ApiOperation(value="add new Siege",response= Siege.class)
    @PostMapping("/add")
    public Siege add(@Valid @RequestBody SiegeRequest siegeRequest){
        Siege siege = new Siege();
        siege.setBus(busRepository.findById(siegeRequest.getIdBus()).orElse(null));
        siege.setDateCreation(new Date());
        siege.setNote(siegeRequest.getNote());
        siege.setNumero(siegeRequest.getNumero());
        siege.setPhoto(siegeRequest.getPhoto());
        siege.setPosition(siegeRequest.getPosition());
        return siegeRepository.save(siege);
    }

    @ApiOperation(value="update Siege by id",response= Siege.class)
    @PutMapping("/update/{id}")
    public Siege add(@PathVariable long id, @Valid @RequestBody SiegeRequest siegeRequest){
        Siege siege = siegeRepository.findById(id).orElse(null);
        siege.setBus(busRepository.findById(siegeRequest.getIdBus()).orElse(null));
        siege.setNote(siegeRequest.getNote());
        siege.setNumero(siegeRequest.getNumero());
        siege.setPhoto(siegeRequest.getPhoto());
        siege.setPosition(siegeRequest.getPosition());
        return siegeRepository.save(siege);
    }

    @ApiOperation(value="delete Siege by id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id){
        siegeRepository.deleteById(id);
    }

}
